/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lanayacomputindo.vcm.vcm_apps.Activity.Gcm;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.clover_studio.spikachatmodule.ChatActivity;
import com.clover_studio.spikachatmodule.models.User;
import com.clover_studio.spikachatmodule.utils.Const;
import com.google.android.gms.gcm.GcmListenerService;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DaftarGuruOnlineActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilNewsActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilUndanganActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Dialog.PopupDialogBerita;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Dialog.PopupDialogGreetings;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Dialog.PopupDialogUndangan;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    private boolean islogin;

    private SharedPreferences sharedPreferences;

    private int notifundangan, notifkabar;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            islogin = sharedPreferences.getBoolean("islogin", false);
        } else {
            islogin = false;
        }
    }

    public void loadSetting()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean checkBoxDiskon = prefs.getBoolean("set_notif_undangan", true);
        boolean checkBoxKabar = prefs.getBoolean("set_notif_kabar", true);

        if(checkBoxDiskon)
        {
            notifundangan = 1;
        }
        else
        {
            notifundangan = 0;
        }

        if(checkBoxKabar)
        {
            notifkabar = 1;
        }
        else
        {
            notifkabar = 0;
        }
    }

    /**
     * Dipanggil ketika menerima message.
     *
     * @param from Sender ID dari pengirim
     * @paramdata data bundle yang mengandung key dan value berdasarkan.
     * data yang dikirimkan oleh GCM */

    @Override
    public void onMessageReceived(String from, Bundle b) {
        //Mengambil data sesuai parameter yang dikirimkan webservice
        loadPreferences();
        loadSetting();

        String param = b.getString("param");

        Log.d(TAG, "param: " + param);

        String sender = b.getString("sender");

        if(param.equalsIgnoreCase("berita")) {
            String id = b.getString("idnews");
            String judul = b.getString("judul");
            String isi = b.getString("isi");
            String create = b.getString("create");
            String photo = b.getString("photo");
            String merchant = b.getString("merchant");

            Log.d(TAG, "id: " + id);
            Log.d(TAG, "photo: " + photo);

            if(islogin && notifkabar == 1) {
                sendNotificationBerita(id, judul, isi, create, photo, merchant);
            }
        }
        else if(param.equalsIgnoreCase("undangan")) {
            String id = b.getString("idundangan");
            String judul = b.getString("judul");
            String deskripsi = b.getString("deskripsi");
            String status = b.getString("status");
            String create = b.getString("create");
            String merchant = b.getString("merchant");

            Log.d(TAG, "id: " + id);

            if(islogin && notifundangan == 1) {
                sendNotificationUndangan(id, judul, deskripsi,status, create, merchant);
            }
        }
        else if(param.equalsIgnoreCase("chat")) {
            String userID = b.getString("userID");
            String name = b.getString("name");
            String avatarURL = b.getString("avatarURL");
            String roomID = b.getString("roomID");
            String roomName = b.getString("roomName");
            String subjectID = b.getString("subjectID");
            String subjectName = b.getString("subjectName");
            String className = b.getString("className");
            String type = b.getString("type");
            String message = b.getString("message");

            Log.d(TAG, "id: " + userID);

            if(islogin) {
                sendNotificationChat(userID, name, avatarURL, roomID, roomName, subjectID, subjectName, className, type, message);
            }
        }
        else if(param.equalsIgnoreCase("greeting")) {

            String id_greeting = b.getString("id_greeting");
            String judul = b.getString("judul");
            String isi = b.getString("isi");
            String status = b.getString("status");
            String photo = b.getString("photo");
            String create = b.getString("create");

            Log.d(TAG, "id: " + id_greeting);
            Log.d(TAG, "photo: " + photo);

            if(islogin) {
                sendNotificationGreetings(id_greeting, judul, isi, status, photo, create);
            }
        }

        Log.d(TAG, "from: " + from);
        Log.d(TAG, "sender: " + sender);
    }
    /**
     * Membuat dan menampilkan notifikasi sederhana yang mengandung pesan GCM.
     */

    private void sendNotificationBerita(String id,String judul,String isi,String create,String photo,String merchant) {
        Intent intent = new Intent(this, DetilNewsActivity.class);

        Bundle b = new Bundle();
        b.putString("idnews", id);
        b.putString("judul", judul);
        b.putString("isi", isi);
        b.putString("create", create);
        b.putString("photo", photo);
        b.putString("merchant", merchant);

        intent.putExtras(b);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_merchant)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_merchant))
                .setContentTitle(getString(R.string.kabar))
                .setContentText(judul)
                .setAutoCancel(true)
                .setTicker(judul)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }

    private void sendNotificationUndangan(String id,String judul,String deskripsi, String status,String create,String merchant) {
        Intent intent = new Intent(this, DetilUndanganActivity.class);
        // memanggil intent yang menunjuk class yang akan dibuka

        Bundle b = new Bundle();
        b.putString("idnews", id);
        b.putString("judul", judul);
        b.putString("deskripsi", deskripsi);
        b.putString("status", status);
        b.putString("create", create);
        b.putString("merchant", merchant);

        intent.putExtras(b);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); //
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_merchant)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_merchant))
                .setContentTitle(getString(R.string.undangan))
                .setContentText(judul)
                .setAutoCancel(true)
                .setTicker(judul)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }

    private void sendNotificationChat(String userID, String namesender, String avatarURL, String roomID, String roomName, String subjectID, String subjectName, String className, String type, String message) {
        SharedPreferences sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        int id = sharedPreferences.getInt("id", 0);
        String token  = sharedPreferences.getString("token", "");
        String name = sharedPreferences.getString("name", "");
        String photo = sharedPreferences.getString("photo", "");
        String gcm_server = sharedPreferences.getString("gcm_server", "");
        User user = new User();
        user.userID = String.valueOf(id);
        user.name = name;
        user.avatarURL = BuildConfig.BASE_API_URL + "/" + photo.replace("\\", "/");
        user.roomID = roomID;
        user.roomName = name;
        user.subjectID = subjectID;
        user.subjectName = subjectName;
        user.className = className;
        user.gcm = token;
        user.gcmServer = gcm_server;

        ChatActivity.name_title = namesender;

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(Const.Extras.USER, user);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent backIntent = new Intent(this, DaftarGuruOnlineActivity.class);
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle b = new Bundle();
        b.putString("id_subject",subjectID);
        b.putString("name_subject",subjectName);
        b.putString("class",className);
        backIntent.putExtras(b);

        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0, new Intent[] {backIntent, intent},
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo_merchant)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.logo_merchant))
                .setContentTitle(namesender)
                .setContentText(message)
                .setAutoCancel(true)
                .setTicker(message)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(Integer.valueOf(userID), notificationBuilder.build());
        Log.e("userID", userID);
    }

    private void sendNotificationGreetings(String id_greeting, String judul, String isi, String status, String photo, String create) {

        Bundle b = new Bundle();
        b.putString("id_greeting", id_greeting );
        b.putString("judul", judul);
        b.putString("isi", isi);
        b.putString("status", status);
        b.putString("photo", photo);
        b.putString("create", create);

        Intent i = new Intent();
        i.putExtras(b);
        i.setClass(this, PopupDialogGreetings.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }
}
