package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentImage;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.MyPageIndicator;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class DetilAlumniActivity extends AppCompatActivity {

    private Intent shareIntent;

    private TextView txtdeskripsi, txtjudul;

    private ImageView imgphoto;

    private String bundleid,bundleproduk,bundledestatus,bundledeskripsi,bundlesize,bundletgl_create;

    private static String nama;

    static ArrayList<String> listPhoto = new ArrayList<String>();

    CollapsingToolbarLayout collapsingToolbarLayout;

    ViewPager mPager;
    LinearLayout mLinearLayout;
    CustomPagerAdapter2 mAdapter;
    MyPageIndicator mIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_extra);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("id");
        bundleproduk = b.getString("judul");
        bundledeskripsi = b.getString("deskripsi");
        bundledestatus = b.getString("status");
        bundletgl_create = b.getString("tgl_create");

        bundlesize = b.getString("size");

        nama = bundleproduk;

        listPhoto.clear();

        for(int i = 0 ; i< Integer.valueOf(bundlesize); i++)
        {
            listPhoto.add(b.getString("asset"+String.valueOf(i)));
        }

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(bundleproduk);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        getSupportActionBar().setTitle(bundleproduk);


        txtjudul = (TextView) findViewById(R.id.judul);
        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);
        imgphoto = (ImageView) findViewById(R.id.photo);

        if(listPhoto.size()>0) {
            Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + listPhoto.get(0).replace("\\", "/"))
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(imgphoto);
        }

        txtjudul.setText(bundleproduk);
        txtdeskripsi.setText(Html.fromHtml(bundledeskripsi));

        mPager = (ViewPager) findViewById(R.id.pager);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);
        mAdapter = new CustomPagerAdapter2(getSupportFragmentManager(), listPhoto);
        mPager.setAdapter(mAdapter);
        mIndicator = new MyPageIndicator(DetilAlumniActivity.this, mLinearLayout, mPager, R.drawable.indicator_circle);
        mIndicator.setPageCount(listPhoto.size());
        mIndicator.show();

    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    public void setupShareIntent() {
        Uri bmpUri = getLocalBitmapUri(imgphoto);
        shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, bundleproduk);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share ke media sosial . ."));
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                DetilAlumniActivity.this.supportFinishAfterTransition();

                return true;

            case R.id.menu_item_share:

                setupShareIntent();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIndicator.cleanup();
    }

    static class CustomPagerAdapter2 extends FragmentPagerAdapter {

        ArrayList<String> mFrags = new ArrayList<>();

        public CustomPagerAdapter2(FragmentManager fm, ArrayList<String> frags) {
            super(fm);
            mFrags = frags;
        }

        @Override
        public Fragment getItem(int position) {
            int index = position % mFrags.size();
            return FragmentImage.newInstance(mFrags.get(index),nama);
        }

        @Override
        public int getCount() {
            return mFrags.size();
        }

        @Override
        public Parcelable saveState()
        {
            return null;
        }
    }

}
