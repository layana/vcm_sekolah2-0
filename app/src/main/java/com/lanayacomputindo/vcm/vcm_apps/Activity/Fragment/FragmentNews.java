package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentNews extends Fragment {

    public int flagkoneksi = 0;

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_guru, container, false);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        ViewPager viewPager = (ViewPager) v.findViewById(R.id.tabanim_viewpager);
        adapter.addFrag(new FragmentPengumumanSekolah(), "Sekolah");
        adapter.addFrag(new FragmentPengumumanKelas(), "Kelas");
        adapter.addFrag(new FragmentPengumumanWalimurid(), "Wali murid");
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tabanim_tabs);
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return mFragmentTitleList.get(position);
            //return null;
        }
    }

}