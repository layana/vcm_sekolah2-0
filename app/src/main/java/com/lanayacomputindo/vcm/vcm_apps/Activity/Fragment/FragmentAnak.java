package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.TambahAnakActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListAnakAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentAnak extends Fragment {

    RecyclerView mRecyclerView;
    GridLayoutManager  mLayoutManager;
    private ListAnakAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    private ArrayList<Person> listanak = new ArrayList<Person>();

    Call<Results<Person>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    private int limit = 10;
    int flagmasihadadata = 0;

    private SearchView mSearchView;

    public int flagkoneksi = 0;

    ArrayList<Person> listPertama;

    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 2);

        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                //Log.d("flagprog",String.valueOf(mAdapter.flagprog));
                if(mAdapter.flagprog == 0) {

                    mAdapter.setProgressOffSpan();
                    return 2;
                }
                else {
                    return 1;
                }
            }
        });

        mRecyclerView.setLayoutManager(mLayoutManager);

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
            }
        });

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
        loadDataFromServer();
    }

    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listanak.clear();
        service = RestClient.getClient(getActivity());
        String with = "personClass.classRoom.branch";
        call = service.getAnakku(with);

        call.enqueue(new Callback<Results<Person>>() {
            @Override
            public void onResponse(Call<Results<Person>> call, Response<Results<Person>> response) {
                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Load Anak Pertama", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)

                    Results<Person> result = response.body();
                    Log.d("Load Anak Pertama", "response = " + new Gson().toJson(result));
                    listanak = result.getData();

                    listPertama = result.getData();

                    //new AddKatalogToDb().execute();

                    mAdapter = new ListAnakAdapter(v.getContext(), listanak, mRecyclerView, FragmentAnak.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Person>> call,Throwable t) {

                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_anak, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case R.id.menu_add_anak:

                Intent i = new Intent(getActivity(), TambahAnakActivity.class);
                startActivity(i);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
