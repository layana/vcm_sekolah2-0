package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilUndanganActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Invitation;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.SubjectTeacher;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListJadwalAdapter extends RecyclerView.Adapter<ListJadwalAdapter.ViewHolder> {

    List<SubjectTeacher> listPost;

    private Context context;

    public Activity activity;
    public ListJadwalAdapter(Context context, ArrayList<SubjectTeacher> listPost, Activity activity) {
        super();

        this.context = context;
        this.activity = activity;
        this.listPost= listPost;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.jadwal_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        SubjectTeacher modelpost = listPost.get(i);
        viewHolder.txtjudul.setText(modelpost.getSubject().getName());
        viewHolder.txttanggal.setText(modelpost.getStart() + " - " + modelpost.getFinished());
        viewHolder.txtguru.setText("Guru : "+modelpost.getPerson().getName());

        //getdata
        viewHolder.currentItem = listPost.get(i);

    }

    @Override
    public int getItemCount() {
        return listPost.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtjudul, txttanggal, txtguru;

        public SubjectTeacher currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtjudul = (TextView)itemView.findViewById(R.id.judul);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);
            txtguru = (TextView)itemView.findViewById(R.id.guru);
        }

    }
}