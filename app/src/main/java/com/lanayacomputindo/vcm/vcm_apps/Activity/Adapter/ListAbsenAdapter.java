package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilKalenderActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.TimeFormater;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListAbsenAdapter extends RecyclerView.Adapter<ListAbsenAdapter.ViewHolder> {

    List<Post> listPost;

    private Context context;

    public ListAbsenAdapter(Context context, ArrayList<Post> listPost) {
        super();

        this.context = context;
        this.listPost = listPost;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.kalender_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Post modelpost = listPost.get(i);
        viewHolder.txtjudul.setText(modelpost.getTitle());
        //viewHolder.txttanggal.setText(modelpost.getStart_date());
        viewHolder.txttanggal.setText(modelpost.getContent());

        //getdata
        viewHolder.currentItem = listPost.get(i);

    }



    @Override
    public int getItemCount() {
        return listPost.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtjudul, txttanggal;

        public Post currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtjudul = (TextView)itemView.findViewById(R.id.judul);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);
        }

    }
}