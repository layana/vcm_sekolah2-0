package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pradana on 18/10/16.
 */

public class SubjectTeacher {
    private Integer id;
    private String name;
    private Integer person_id;
    private Integer class_room_id;
    private Integer subject_id;
    private String start;
    private String finished;
    private String day;
    private Integer status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Person person;
    private Subject subject;
    private ClassRoom class_room;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The person_id
     */
    public Integer getPerson_id() {
        return person_id;
    }

    /**
     *
     * @param person_id
     * The person_id
     */
    public void setPerson_id(Integer person_id) {
        this.person_id = person_id;
    }

    /**
     *
     * @return
     * The class_room_id
     */
    public Integer getClass_room_id() {
        return class_room_id;
    }

    /**
     *
     * @param class_room_id
     * The class_room_id
     */
    public void setClass_room_id(Integer class_room_id) {
        this.class_room_id = class_room_id;
    }

    /**
     *
     * @return
     * The subject_id
     */
    public Integer getSubject_id() {
        return subject_id;
    }

    /**
     *
     * @param subject_id
     * The subject_id
     */
    public void setSubject_id(Integer subject_id) {
        this.subject_id = subject_id;
    }

    /**
     *
     * @return
     * The start
     */
    public String getStart() {
        return start;
    }

    /**
     *
     * @param start
     * The start
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     *
     * @return
     * The finished
     */
    public String getFinished() {
        return finished;
    }

    /**
     *
     * @param finished
     * The finished
     */
    public void setFinished(String finished) {
        this.finished = finished;
    }

    /**
     *
     * @return
     * The day
     */
    public String getDay() {
        return day;
    }

    /**
     *
     * @param day
     * The day
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     *
     * @return
     * The status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The person
     */
    public Person getPerson() {
        return person;
    }

    /**
     *
     * @param person
     * The person
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    /**
     *
     * @return
     * The subject
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     *
     * @param subject
     * The subject
     */
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    /**
     *
     * @return
     * The class_room
     */
    public ClassRoom getClass_room() {
        return class_room;
    }

    /**
     *
     * @param class_room
     * The class_room
     */
    public void setClass_room(ClassRoom class_room) {
        this.class_room = class_room;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
