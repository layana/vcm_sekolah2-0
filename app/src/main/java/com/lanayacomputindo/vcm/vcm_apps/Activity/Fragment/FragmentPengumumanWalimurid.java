package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListPengumumanWalimuridAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.OnLoadMoreListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentPengumumanWalimurid extends Fragment {

    private int limit = 10;
    RecyclerView mRecyclerView;
    ListPengumumanWalimuridAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    ArrayList<Post> listNews = new ArrayList<Post>();

    Call<Results<Post>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    public int flagmasihadadata = 0;

    public int flagkoneksi = 0;

    ArrayList<Post> listPertama;

    View v;

    int id,client_id, branch_id;
    boolean status;
    String name,email,birthday,religion,phone,address,photo,created_at,updated_at;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getInt("id", 0);
            client_id = sharedPreferences.getInt("client_id", 0);
            name = sharedPreferences.getString("name", "");
            email = sharedPreferences.getString("email", "");
            birthday = sharedPreferences.getString("birthday", "");
            religion = sharedPreferences.getString("religion", "");
            phone = sharedPreferences.getString("phone", "");
            address = sharedPreferences.getString("address", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getBoolean("status", false);
            created_at = sharedPreferences.getString("created_at", "");
            updated_at = sharedPreferences.getString("updated_at", "");
            branch_id = sharedPreferences.getInt("branch_id", 0);
        } else {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        loadPreferences();

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
            }
        });

        return v;
    }

    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listNews.clear();
        service = RestClient.getClient(getActivity());
        String with = "asset";
        String orderBy = "id";
        String sortedBy = "desc";
        call = service.getMyNews(with, String.valueOf(0), String.valueOf(limit), orderBy, sortedBy);

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call,Response<Results<Post>> response) {

                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Pengmuman Walimurid", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Post> result = response.body();
                    Log.d("Pengmuman Walimurid", "response = " + new Gson().toJson(result));
                    listNews = result.getData();

                    listPertama = result.getData();

                    mAdapter = new ListPengumumanWalimuridAdapter(v.getContext(),listNews, mRecyclerView, FragmentPengumumanWalimurid.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {

                        if(listPertama.size()<limit) {

                        }
                        else
                        {
                            loadMoreDataFromServer();

                        }

                        }
                    });

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call,Throwable t) {
                listNews.clear();
                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("walimurid", t.getLocalizedMessage());
                Toast.makeText(v.getContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();

            }
        });
    }

    public void deleteItem()
    {
        listNews.remove(listNews.size() - 1);
        mAdapter.deleteItem(listNews.size()-1);
    }

    public void loadMoreDataFromServer()
    {
        if (flagmasihadadata == 0) {

            mAdapter.addItem(null, listNews.size()-1);

            int index = mAdapter.getItemCount() - 1;

            Log.d("indexny", String.valueOf(index));
            service = RestClient.getClient(getActivity());
            String with = "asset";
            String orderBy = "id";
            String sortedBy = "desc";
            call = service.getMyNews(with, String.valueOf(index), String.valueOf(limit), orderBy, sortedBy);

            call.enqueue(new Callback<Results<Post>>() {

                @Override
                public void onResponse(Call<Results<Post>> call,Response<Results<Post>> response) {
                    flagkoneksi = 0;
                    mAdapter.deleteItem(listNews.size()-1);

                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("Pengmuman Walimurid", "Status Code = " + response.code());
                    if (response.isSuccessful()) {
                        // request successful (status code 200, 201)
                        Results<Post> result = response.body();
                        Log.d("Pengmuman Walimurid", "response = " + new Gson().toJson(result));
                        ArrayList<Post> listTemp;
                        listTemp = result.getData();
                        if (listTemp.size() < limit) {
                            flagmasihadadata = 1;
                        }
                        for (int i = 0; i < listTemp.size(); i++) {
                            mAdapter.addItem(listTemp.get(i), listNews.size() - 1);
                        }
                        mAdapter.setLoaded();

                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                    }
                }

                @Override
                public void onFailure(Call<Results<Post>> call,Throwable t) {
                    flagkoneksi = 1;
                    mSwipeRefreshLayout.setRefreshing(false);
                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("walimurid", t.getLocalizedMessage());
                    Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();

                    mAdapter.deleteItem(listNews.size()-1);

                    mAdapter.addItem(null, listNews.size() - 1);
                }
            });
        }
    }

}