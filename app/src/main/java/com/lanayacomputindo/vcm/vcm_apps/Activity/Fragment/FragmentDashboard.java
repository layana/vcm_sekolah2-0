package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.AxisValueFormatter;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilNewsActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.ResizeWidthAnimation;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.SubjectTeacher;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Value;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDashboard extends Fragment {

    private SharedPreferences sharedPreferences;
    private RestClient.GitApiInterface service;
    private LineChart chart;
    private List<Entry> entries;
    private LineDataSet dataSet;
    private int personId;
    private int branchId;
    private int subjectId;
    private String anak_name, anak_kelas, anak_foto;

    private int maxProgress, sizeSubject = 0, currentSubject = 0;
    private float persen;

    private TextView txtnama, txtkelas, txtsubject, txtteacher, txtpersen, txtday;
    private CircleImageView imgfoto;
    private LinearLayout llprogress, llprogressmax;
    private Button btnSenin, btnSelasa, btnRabu, btnKamis, btnJumat, btnSabtu, btnSatu, btnDua, btnTiga, btnEmpat, btnLima, btnEnam, btnTujuh;

    private View dialogView;
    private SwipeRefreshLayout mSwipeRefreshLayout ;
    private ProgressDialog mProgressDialog;
    private TextView txtChartTitle;
    private YAxis yAxis;
    private XAxis xAxis;
    private List<String> xValues = new ArrayList<String>();
    private Button btnFilter;
    private int countValue;
    private List<Value> listSubject;

    private LinearLayout layoutInfoLain;
    private LinearLayout layoutInfoUtama;
    private TextView textInfoDashboard;
    private ImageView imageInfoDashboard;
    private Intent intentInfoUtama;
    private Bundle bundleInfoUtama;

    public void loadPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            personId = sharedPreferences.getInt("anak_id", 0);
            branchId = sharedPreferences.getInt("branch_id", 0);
            anak_name = sharedPreferences.getString("anak_name", "");
            anak_kelas = sharedPreferences.getString("anak_kelas", "");
            anak_foto = sharedPreferences.getString("anak_foto", "");
        } else {

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadPreferences();

        Log.e("id_anak", String.valueOf(personId));
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(true);
        mProgressDialog.setCanceledOnTouchOutside(false);

        entries = new ArrayList<Entry>();
        listSubject = new ArrayList<Value>();
        intentInfoUtama = new Intent(getActivity(), DetilNewsActivity.class);
        bundleInfoUtama = new Bundle();
        showChart();
        showNews();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        chart = (LineChart) v.findViewById(R.id.chart);
        chart.setDescription("");

        chart.getAxisRight().setDrawGridLines(false);
        chart.getAxisRight().setEnabled(false);
        chart.getLegend().setEnabled(false);

        yAxis = chart.getAxisLeft();
        yAxis.enableGridDashedLine(10f, 10f, 0f);
        yAxis.setAxisMaxValue(100f);
        yAxis.setAxisMinValue(0f);
        yAxis.setGranularity(25f);
        yAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.chart_text));
        yAxis.setGridColor(ContextCompat.getColor(getContext(), R.color.chart_text));

        xAxis = chart.getXAxis();
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.setAxisMinValue(-0.2f);
        xAxis.setGranularity(1f);
        xAxis.setTextColor(ContextCompat.getColor(getContext(), R.color.chart_text));
        xAxis.setGridColor(ContextCompat.getColor(getContext(), R.color.chart_text));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        txtChartTitle = (TextView) v.findViewById(R.id.text_chart_title);
        dialogView = inflater.inflate(R.layout.dialog_chart_filter, null);
        btnFilter = (Button) v.findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterDialog();
            }
        });

        imageInfoDashboard = (ImageView) v.findViewById(R.id.image_info_dashboard);
        textInfoDashboard = (TextView) v.findViewById(R.id.text_info_dashboard);
        layoutInfoUtama = (LinearLayout) v.findViewById(R.id.layout_info_utama);
        layoutInfoUtama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bundleInfoUtama.isEmpty())
                    startActivity(intentInfoUtama);
            }
        });
        layoutInfoLain = (LinearLayout) v.findViewById(R.id.layout_info_lain);
        layoutInfoLain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.kabar));
                FragmentNews fragmentNews = new FragmentNews();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.nav_contentframe, fragmentNews);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        llprogress = (LinearLayout) v.findViewById(R.id.llprogress);
        llprogressmax = (LinearLayout) v.findViewById(R.id.llprogressmax);

        txtnama = (TextView) v.findViewById(R.id.txtnama);
        txtkelas = (TextView) v.findViewById(R.id.txtkelas);
        imgfoto = (CircleImageView) v.findViewById(R.id.imgfoto);

        txtday = (TextView) v.findViewById(R.id.txtday);
        txtpersen = (TextView) v.findViewById(R.id.txtpersen);
        txtsubject = (TextView) v.findViewById(R.id.txtsubject);
        txtteacher = (TextView) v.findViewById(R.id.txtteacher);

        btnSenin = (Button) v.findViewById(R.id.btnSenin);
        btnSenin.setTransformationMethod(null);
        btnSelasa = (Button) v.findViewById(R.id.btnSelasa);
        btnSelasa.setTransformationMethod(null);
        btnRabu = (Button) v.findViewById(R.id.btnRabu);
        btnRabu.setTransformationMethod(null);
        btnKamis = (Button) v.findViewById(R.id.btnKamis);
        btnKamis.setTransformationMethod(null);
        btnJumat = (Button) v.findViewById(R.id.btnJumat);
        btnJumat.setTransformationMethod(null);
        btnSabtu = (Button) v.findViewById(R.id.btnSabtu);
        btnSabtu.setTransformationMethod(null);

        btnSatu = (Button) v.findViewById(R.id.btnSatu);
        btnSatu.setVisibility(View.GONE);
        btnDua = (Button) v.findViewById(R.id.btnDua);
        btnDua.setVisibility(View.GONE);
        btnTiga = (Button) v.findViewById(R.id.btnTiga);
        btnTiga.setVisibility(View.GONE);
        btnEmpat = (Button) v.findViewById(R.id.btnEmpat);
        btnEmpat.setVisibility(View.GONE);
        btnLima = (Button) v.findViewById(R.id.btnLima);
        btnLima.setVisibility(View.GONE);
        btnEnam = (Button) v.findViewById(R.id.btnEnam);
        btnEnam.setVisibility(View.GONE);
        btnTujuh = (Button) v.findViewById(R.id.btnTujuh);
        btnTujuh.setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                getSubjectToday();
                setDataAnak();
                setButtonDay();
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        getSubjectToday();
        setDataAnak();
        setButtonDay();
    }

    public void showFilterDialog() {
        listSubject.clear();
        final List<String> myList = new ArrayList<String>();
        String with = "subject";
        String search = "person_id:" + String.valueOf(personId);
        // String search = "person_id:34;subject_id:3";
        String orderBy = "subject_id";
        String sortedBy = "asc";

        final Spinner spinnerFilter = (Spinner) dialogView.findViewById(R.id.spinner_chart_filter);

        Call<Results<Value>> call = service.getValue(with, search, orderBy, sortedBy);
        call.enqueue(new Callback<Results<Value>>() {
            @Override
            public void onResponse(Call<Results<Value>> call, Response<Results<Value>> response) {
                if (response.isSuccessful()) {
                    Value value = new Value();
                    value.setSubject_id(0);
                    listSubject.add(value);
                    myList.add("Pilih Mata Pelajaran");
                    if (response.body().getData().size()>0) {
                        int subjectId = 0;
                        for (Value data : response.body().getData()) {
                            if (subjectId==0 || data.getSubject_id()!=subjectId) {
                                listSubject.add(data);
                                myList.add(data.getSubject().getName());
                                subjectId = data.getSubject_id();
                            }
                        }
                    } else {

                    }

                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item, myList) {
                        @Override
                        public boolean isEnabled(int position){
                            if(position == 0)
                            {
                                // Disable the first item from Spinner
                                // First item will be use for hint
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        @Override
                        public View getDropDownView(int position, View convertView,
                                                    ViewGroup parent) {
                            View view = super.getDropDownView(position, convertView, parent);
                            TextView tv = (TextView) view;
                            if(position == 0){
                                // Set the hint text color gray
                                tv.setTextColor(Color.GRAY);
                            }
                            else {
                                tv.setTextColor(Color.BLACK);
                            }
                            return view;
                        }
                    };
                    spinnerFilter.setAdapter(dataAdapter);
                }
            }

            @Override
            public void onFailure(Call<Results<Value>> call, Throwable t) {

            }
        });

        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                subjectId = listSubject.get(i).getSubject_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        if(dialogView.getParent()!=null)
            ((ViewGroup)dialogView.getParent()).removeView(dialogView); // <- fix
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle("Mata Pelajaran");
        dialogBuilder.setPositiveButton("Tampilkan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                showChart();
            }
        });
        dialogBuilder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setDataAnak()
    {
        txtnama.setText(anak_name);
        txtkelas.setText("Kelas "+anak_kelas);
        Picasso.with(getActivity()).load(BuildConfig.BASE_API_URL + "/" + anak_foto.replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(imgfoto);
    }

    public void setProgress()
    {
        llprogressmax.post(new Runnable() {
            @Override
            public void run() {
                maxProgress = llprogressmax.getWidth();
                persen = (maxProgress / sizeSubject) * currentSubject;
                int persenan = (100 / sizeSubject) * currentSubject;
                Log.e("size", String.valueOf(maxProgress) + " " + String.valueOf((int)persen) + " " + String.valueOf(sizeSubject) + " " + String.valueOf(currentSubject));
                ResizeWidthAnimation anim = new ResizeWidthAnimation(llprogress, (int)persen);
                anim.setDuration(500);
                llprogress.startAnimation(anim);
                if(persenan>98)
                {
                    persenan = 100;
                }
                txtpersen.setText(persenan+"%");
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) llprogress.getLayoutParams();
                llprogress.setLayoutParams(lp);
            }
        });
    }

    public void setButtonDay()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.US);
        Date d = new Date();
        String day = sdf.format(d);
        Log.e("day", day + "zumba");
        if(day.equalsIgnoreCase("Monday"))
        {
            btnSenin.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Senin");
        }
        if(day.equalsIgnoreCase("Tuesday"))
        {
            btnSelasa.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Selasa");
        }
        if(day.equalsIgnoreCase("Wednesday"))
        {
            btnRabu.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Rabu");
        }
        if(day.equalsIgnoreCase("Thursday"))
        {
            btnKamis.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Kamis");
        }
        if(day.equalsIgnoreCase("Friday"))
        {
            btnJumat.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Jumat");
        }
        if(day.equalsIgnoreCase("Saturday"))
        {
            btnSabtu.setBackgroundResource(R.drawable.custom_border_button_active);
            txtday.setText("Sabtu");
        }
    }

    private void getSubjectToday() {
        service = RestClient.getClient(getActivity());

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.US);
        Date d = new Date();
        String day = sdf.format(d);

        Log.e("SubjectTeacher", day);

        String with = "subject;person;classRoom;classRoom.personClass";
        String search = "day:"+day.toLowerCase()+";classRoom.personClass.person_id:"+personId;
        String orderBy = "start";
        String sortedBy = "asc";

        Call<Results<SubjectTeacher>> call = service.getSubjectTeacherToday(with, search, orderBy, sortedBy);
        call.enqueue(new Callback<Results<SubjectTeacher>>() {
            @Override
            public void onResponse(Call<Results<SubjectTeacher>> call, Response<Results<SubjectTeacher>> response) {
                if (response.isSuccessful()) {
                    Results<SubjectTeacher> result = response.body();
                    Log.e("SubjectTeacher", "response = " + new Gson().toJson(result));
                    setMataPelajaranAktif(response.body().getData());
                    mSwipeRefreshLayout.setRefreshing(false);
                }
            }
            @Override
            public void onFailure(Call<Results<SubjectTeacher>> call, Throwable t) {
                Log.e("Subject Teacher", t.toString());
                Toast.makeText(getActivity(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setXAxisLabel() {
        xAxis.setValueFormatter(new AxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                //return xValues[(int) value % xValues.length];
                return xValues.get((int) value % xValues.size());
            }

            @Override
            public int getDecimalDigits() {
                return 0;
            }
        });
    }

    private void showNews() {
        String with = "asset";
        String orderBy = "id";
        String sortedBy = "desc";
        String limit = "1";
        Call<Results<Post>> call = service.getBranchNewsDashboard(branchId, with, orderBy, sortedBy, limit);
        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData().size()>0) {
                        textInfoDashboard.setText(response.body().getData().get(0).getTitle());

                        bundleInfoUtama.putString("idnews", String.valueOf(response.body().getData().get(0).getId()));
                        bundleInfoUtama.putString("judul", String.valueOf(response.body().getData().get(0).getTitle()));
                        bundleInfoUtama.putString("isi", String.valueOf(response.body().getData().get(0).getContent()));
                        bundleInfoUtama.putString("create", String.valueOf(response.body().getData().get(0).getCreated_at()));
                        bundleInfoUtama.putString("size", String.valueOf(response.body().getData().get(0).getAsset().size()));

                        if (response.body().getData().get(0).getAsset().size()>0) {
                            String photo = response.body().getData().get(0).getAsset().get(0).getFile();
                            if (!photo.equals("")) {
                                photo = photo.replace("\\", "/");
                                Picasso.with(getActivity()).load(BuildConfig.BASE_API_URL + "/" + photo)
                                        .error(R.drawable.loadimage)
                                        .placeholder(R.drawable.loadimage)
                                        .into(imageInfoDashboard);
                            }
                            bundleInfoUtama.putString("photo", photo);

                            /*for(int i = 0 ; i< response.body().getData().get(0).getAsset().size(); i++)
                            {
                                Log.e("asset"+String.valueOf(i), response.body().getData().get(0).getAsset().get(i).getFile());
                                bundleInfoUtama.putString("asset"+String.valueOf(i), response.body().getData().get(0).getAsset().get(i).getFile());
                            }*/
                        }

                        intentInfoUtama.putExtras(bundleInfoUtama);
                    }
                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call, Throwable t) {

            }
        });
    }


    public void setMataPelajaranAktif(ArrayList<SubjectTeacher> listSubjectToday)
    {
        if(listSubjectToday.size()>0)
        {
            if(listSubjectToday.size()==1)
            {
                btnSatu.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==2)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==3)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
                btnTiga.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==4)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
                btnTiga.setVisibility(View.VISIBLE);
                btnEmpat.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==5)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
                btnTiga.setVisibility(View.VISIBLE);
                btnEmpat.setVisibility(View.VISIBLE);
                btnLima.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==6)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
                btnTiga.setVisibility(View.VISIBLE);
                btnEmpat.setVisibility(View.VISIBLE);
                btnLima.setVisibility(View.VISIBLE);
                btnEnam.setVisibility(View.VISIBLE);
            }
            if(listSubjectToday.size()==7)
            {
                btnSatu.setVisibility(View.VISIBLE);
                btnDua.setVisibility(View.VISIBLE);
                btnTiga.setVisibility(View.VISIBLE);
                btnEmpat.setVisibility(View.VISIBLE);
                btnLima.setVisibility(View.VISIBLE);
                btnEnam.setVisibility(View.VISIBLE);
                btnTujuh.setVisibility(View.VISIBLE);
            }

            Calendar now = Calendar.getInstance();
            int hour = now.get(Calendar.HOUR_OF_DAY);
            int minute = now.get(Calendar.MINUTE);
            Date date = parseDate(hour + ":" + minute);

            int hourAwalSubject = Integer.parseInt(listSubjectToday.get(0).getStart().substring(0, 2));
            int hourAkhirSubject = Integer.parseInt(listSubjectToday.get(listSubjectToday.size()-1).getFinished().substring(0, 2));
            int minuteAkhirSubject = Integer.parseInt(listSubjectToday.get(listSubjectToday.size()-1).getFinished().substring(3, 5));

            Log.e("awal",String.valueOf(hour)+" "+String.valueOf(hourAwalSubject)+" "+String.valueOf(hourAkhirSubject));
            if(hour<hourAwalSubject)
            {
                currentSubject = 0;
                Log.e("awal","awal");
                txtsubject.setText(listSubjectToday.get(0).getName());
                txtteacher.setText(listSubjectToday.get(0).getPerson().getName());
                subjectId = listSubjectToday.get(0).getSubject().getId();
                showChart();
            }
            else if(hour>=hourAkhirSubject && minute>minuteAkhirSubject)
            {
                currentSubject = listSubjectToday.size();
                Log.e("akhir","akhir");
                txtsubject.setText(listSubjectToday.get(listSubjectToday.size()-1).getName());
                txtteacher.setText(listSubjectToday.get(listSubjectToday.size()-1).getPerson().getName());
                subjectId = listSubjectToday.get(listSubjectToday.size()-1).getSubject().getId();
                showChart();
                int i = 0;
                for(SubjectTeacher subject : listSubjectToday)
                {
                    i++;
                    if(i==1)
                    {
                        btnSatu.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==2)
                    {
                        btnDua.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==3)
                    {
                        btnTiga.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==4)
                    {
                        btnEmpat.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==5)
                    {
                        btnLima.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==6)
                    {
                        btnEnam.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==7)
                    {
                        btnTujuh.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                }
            }
            else
            {
                int i = 0;
                for(SubjectTeacher subject : listSubjectToday)
                {
                    i++;
                    String start = subject.getStart();
                    String finish = subject.getFinished();
                    Date startDate = parseDate(start);
                    Date finishDate = parseDate(finish);
                    if (startDate.before( date ) && finishDate.after(date)) {

                        Log.e("dalam","dalam");
                        txtsubject.setText(subject.getName());
                        txtteacher.setText(subject.getPerson().getName());
                        subjectId = subject.getSubject().getId();
                        showChart();
                        if(i==1)
                        {
                            btnSatu.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==2)
                        {
                            btnDua.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==3)
                        {
                            btnTiga.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==4)
                        {
                            btnEmpat.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==5)
                        {
                            btnLima.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==6)
                        {
                            btnEnam.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        if(i==7)
                        {
                            btnTujuh.setBackgroundResource(R.drawable.custom_circle_active);
                        }
                        currentSubject = i;
                        break;
                    }
                    else if(date.before( startDate ))
                    {
                        txtsubject.setText("BREAK");
                        txtteacher.setText("");
                        currentSubject = i-1;
                        break;
                    }
                    if(i==1)
                    {
                        btnSatu.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==2)
                    {
                        btnDua.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==3)
                    {
                        btnTiga.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==4)
                    {
                        btnEmpat.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==5)
                    {
                        btnLima.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==6)
                    {
                        btnEnam.setBackgroundResource(R.drawable.custom_circle_active);
                    }
                    if(i==7)
                    {
                        btnTujuh.setBackgroundResource(R.drawable.custom_circle_active);
                    }

                }
                Log.e("berjalan","berjalan");
            }
            sizeSubject = listSubjectToday.size();
            setProgress();
        }
        else
        {
            Toast.makeText(getActivity(), "Anak anda belum memiliki mata pelajaran", Toast.LENGTH_SHORT).show();
        }
    }

    private Date parseDate(String date) {
        SimpleDateFormat inputParser = new SimpleDateFormat("HH:mm");
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private void setChartData(List<Entry> entries) {
        xAxis.setAxisMaxValue(countValue-1 + 0.2f);
        dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
        dataSet.setCircleColor(ContextCompat.getColor(getContext(), R.color.chart_line));
        dataSet.setColor(ContextCompat.getColor(getContext(), R.color.chart_line));
        dataSet.setValueTextColor(ContextCompat.getColor(getContext(), R.color.chart_line));
        dataSet.setDrawFilled(true);
        dataSet.setFillColor(ContextCompat.getColor(getContext(), R.color.chart_line));
        dataSet.setFillAlpha(230);
        LineData lineData = new LineData(dataSet);
        chart.setData(lineData);
        chart.invalidate();
    }

    private void showChart() {
        if (xValues.size()>0)
            xValues.clear();
        if (entries.size()>0)
            entries.clear();
        countValue = 0;
        service = RestClient.getClient(getActivity());

        String with = "subject";
        String search = "person_id:" + String.valueOf(personId) + ";subject_id:" + String.valueOf(subjectId);
        // String search = "person_id:34;subject_id:3";
        String orderBy = "id";
        String sortedBy = "asc";

        Call<Results<Value>> call = service.getValue(with, search, orderBy, sortedBy);
        call.enqueue(new Callback<Results<Value>>() {
            @Override
            public void onResponse(Call<Results<Value>> call, Response<Results<Value>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getData().size()>0) {
                        String title = "";
                        for (Value data : response.body().getData()) {
                            try {
                                title = data.getSubject().getName().toString();
                                xValues.add(data.getType().toString());
                                entries.add(new Entry(countValue, Integer.parseInt(data.getValue_number())));
                                countValue++;
                            } catch (Exception e) {

                            }
                        }
                        txtChartTitle.setText("Nilai - " + title);
                        setXAxisLabel();
                        setChartData(entries);
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<Results<Value>> call, Throwable t) {

            }
        });
    }
}
