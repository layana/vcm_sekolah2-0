package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListTeachAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Subject;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Teach;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentDaftarGuruOnline extends Fragment {

    RecyclerView mRecyclerView;
    ListTeachAdapter mAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout ;

    ArrayList<Teach> listTeach = new ArrayList<Teach>();

    Call<Results<Teach>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;
    String id_subject, name_subject, classs;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        id_subject = getArguments().getString("id_subject");
        name_subject = getArguments().getString("name_subject");
        classs = getArguments().getString("class");
        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server

                getGuru();
            }
        });


        getGuru();

        return v;
    }

    public void getGuru()
    {
        service = RestClient.getClient(getActivity());
        String with = "user";
        String search = "user.online:"+"1"+";subject_id:"+id_subject;
        call = service.getTeachOnline(with, search);

        call.enqueue(new Callback<Results<Teach>>() {
            @Override
            public void onResponse(Call<Results<Teach>> call, Response<Results<Teach>> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Teach> result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
                    listTeach = result.getData();
                    Subject subject = new Subject();
                    subject.setId(Integer.valueOf(id_subject));
                    subject.setName(name_subject);
                    mAdapter = new ListTeachAdapter(v.getContext(), listTeach, subject, classs);
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Results<Teach>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.e("on Failure", t.toString());
                Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        getGuru();
    }
}