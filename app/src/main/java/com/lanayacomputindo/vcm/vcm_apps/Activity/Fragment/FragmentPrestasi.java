package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListPrestasiAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.OnLoadMoreListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentPrestasi extends Fragment{

    RecyclerView mRecyclerView;
    GridLayoutManager  mLayoutManager;
    private ListPrestasiAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    private ArrayList<Post> listPost = new ArrayList<Post>();

    Call<Results<Post>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    private int limit = 10;
    int flagmasihadadata = 0;

    private SearchView mSearchView;

    public int flagkoneksi = 0;

    ArrayList<Post> listPertama;

    View v;

    int branch_id;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            branch_id = sharedPreferences.getInt("branch_id", 0);
        } else {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        loadPreferences();

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 2);

        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                //Log.d("flagprog",String.valueOf(mAdapter.flagprog));
                if(mAdapter.flagprog == 0) {

                    mAdapter.setProgressOffSpan();
                    return 2;
                }
                else {
                    return 1;
                }
            }
        });

        mRecyclerView.setLayoutManager(mLayoutManager);

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
            }
        });

        return v;
    }


    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listPost.clear();
        service = RestClient.getClient(getActivity());
        String with = "asset";
        String search = "branch_id:"+branch_id+";type:achievement";
        String searchFields = "branch_id:=;type:=";
        call = service.getPostWithOffset(search, searchFields, with, String.valueOf(0),String.valueOf(limit));

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call, Response<Results<Post>> response) {
                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Load Katalog Pertama", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)

                    Results<Post> result = response.body();
                    Log.d("Load Katalog Pertama", "response = " + new Gson().toJson(result));
                    listPost = result.getData();

                    listPertama = result.getData();

                    mAdapter = new ListPrestasiAdapter(v.getContext(), listPost, mRecyclerView, FragmentPrestasi.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                    Log.d("indexny", String.valueOf(mAdapter.getItemCount() - 1));

                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {

                            if(listPertama.size()<limit) {

                            }
                            else
                            {
                                loadMoreDataFromServer();

                            }

                        }
                    });

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call,Throwable t) {

                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();


            }
        });
    }

    public void loadMoreDataFromServer()
    {

        Log.d("flagmasihadadata", String.valueOf(flagmasihadadata));

        if (flagmasihadadata == 0) {
            mAdapter.addItem(null, listPost.size() - 1);

            int index = mAdapter.getItemCount() - 1;

            Log.d("indexny", String.valueOf(index));


            service = RestClient.getClient(getActivity());
            String with = "asset";
            String search = "branch_id:"+branch_id+";type:achievement";
            String searchFields = "branch_id:=;type:=";
            call = service.getPostWithOffset(search, searchFields, with, String.valueOf(index),String.valueOf(limit));

            call.enqueue(new Callback<Results<Post>>() {

                @Override
                public void onResponse(Call<Results<Post>> call,Response<Results<Post>> response) {
                    flagkoneksi = 0;
                    mAdapter.deleteItem(listPost.size()-1);

                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("Load Katalog Next", "Status Code = " + response.code());
                    if (response.isSuccessful()) {
                        // request successful (status code 200, 201)
                        Results<Post> result = response.body();
                        Log.d("Load Katalog Next aye", "response = " + new Gson().toJson(result));
                        ArrayList<Post> listTemp;
                        listTemp = result.getData();
                        if (listTemp.size() < limit) {
                            flagmasihadadata = 1;
                        }
                        for (int i = 0; i < listTemp.size(); i++) {
                            listPost.add(listTemp.get(i));
                            mAdapter.addItem(listTemp.get(i), listPost.size() - 1);
                        }
                        mAdapter.setLoaded();

                    } else {
                        // response received but request not successful (like 400,401,403 etc)
                        //Handle errors
                    }
                }

                @Override
                public void onFailure(Call<Results<Post>> call,Throwable t) {
                    flagkoneksi = 1;
                    mSwipeRefreshLayout.setRefreshing(false);
                    Progress.setVisibility(View.GONE);
                    progressimage.setVisibility(View.GONE);
                    notfound.setVisibility(View.GONE);
                    Log.d("next gagal", "gagalall");
                    Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();

                    listPost.remove(listPost.size() - 1);
                    mAdapter.deleteItem(listPost.size());

                    mAdapter.setProgressOnSpan();

                    listPost.add(null);
                    mAdapter.addItem(null, listPost.size() - 1);

                    mAdapter.setProgressOffSpan();

                }
            });
        }
    }


    public void deleteItem()
    {
        listPost.remove(listPost.size() - 1);
        mAdapter.deleteItem(listPost.size());
    }

}
