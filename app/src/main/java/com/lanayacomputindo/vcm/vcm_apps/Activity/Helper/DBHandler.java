package com.lanayacomputindo.vcm.vcm_apps.Activity.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

public class DBHandler extends SQLiteOpenHelper {

	public static final String DB_NAME = "db_sekolah_6";
	public static final int DB_VERSION = 1;

	SQLiteDatabase sqliteDB;

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub

        String sqlQueryKalender = "create table if not exists tabel_kalender" +
                " ("+ BaseColumns._ID+" integer primary key autoincrement, " +
                "id_kalender text not null, "+
                "judul text not null, "+
                "deskripsi text not null, "+
                "tgl_mulai text not null, "+
                "status text not null);";

        db.execSQL(sqlQueryKalender);

	}

	public DBHandler(Context context){
		super(context, DB_NAME, null, DB_VERSION);
		sqliteDB = this.getWritableDatabase();
	}




    public void insertKalender(String id_kalender, String judul, String deskripsi, String tgl_mulai, String status){

        ContentValues contentValues = new ContentValues();

        contentValues.put("id_kalender", id_kalender);
        contentValues.put("judul", judul);
        contentValues.put("deskripsi", deskripsi);
        contentValues.put("tgl_mulai", tgl_mulai);
        contentValues.put("status", status);

        sqliteDB.insert("tabel_kalender", null, contentValues);
    }

    public ArrayList<Post> getDataKalender()
    {
        ArrayList<Post> listGuru = new ArrayList<Post>();

        Cursor cursor = sqliteDB.query("tabel_kalender", null, null, null, null, null, null);
        while(cursor.moveToNext())
        {

            String id_kalender = cursor.getString(cursor.getColumnIndex("id_kalender"));
            String judul = cursor.getString(cursor.getColumnIndex("judul"));
            String deskripsi = cursor.getString(cursor.getColumnIndex("deskripsi"));
            String tgl_mulai = cursor.getString(cursor.getColumnIndex("tgl_mulai"));
            String status = cursor.getString(cursor.getColumnIndex("status"));

            Post kalender = new Post();
            kalender.setId(Integer.valueOf(id_kalender));
            kalender.setTitle(judul);
            kalender.setContent(deskripsi);
            kalender.setStart_date(tgl_mulai);
            kalender.setStatus(Boolean.valueOf(status));

            listGuru.add(kalender);
        }

        return listGuru;
    }

    public ArrayList<Post> getKalenderByDate(String tanggal)
    {
        ArrayList<Post> listGuru = new ArrayList<Post>();

        Cursor cursor = sqliteDB.query("tabel_kalender", null, null, null, null, null, null);
        while(cursor.moveToNext())
        {

            String id_kalender = cursor.getString(cursor.getColumnIndex("id_kalender"));
            String judul = cursor.getString(cursor.getColumnIndex("judul"));
            String deskripsi = cursor.getString(cursor.getColumnIndex("deskripsi"));
            String tgl_mulai = cursor.getString(cursor.getColumnIndex("tgl_mulai"));
            String status = cursor.getString(cursor.getColumnIndex("status"));

            if(tanggal.equalsIgnoreCase(tgl_mulai))
            {
                Post kalender = new Post();
                kalender.setId(Integer.valueOf(id_kalender));
                kalender.setTitle(judul);
                kalender.setContent(deskripsi);
                kalender.setStart_date(tgl_mulai);
                kalender.setStatus(Boolean.valueOf(status));

                listGuru.add(kalender);
            }
        }

        return listGuru;
    }

    public void deleteAllKalender(){

        sqliteDB.execSQL("delete from tabel_kalender");
    }



	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
}
