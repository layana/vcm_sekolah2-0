package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Result<D> {
    private Boolean success;
    private D data;
    private String message;

    /**
     *
     * @return
     * The success
     */
    public Boolean getSuccess() {
        return success;
    }

    /**
     *
     * @param success
     * The success
     */
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    /**
     *
     * @return
     * The data
     */
    public D getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(D data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }


    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
