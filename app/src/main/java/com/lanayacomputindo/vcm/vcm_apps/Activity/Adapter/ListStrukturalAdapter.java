package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilGuruActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentStruktural;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.OnLoadMoreListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListStrukturalAdapter extends RecyclerView.Adapter{

    public final int VIEW_ITEM = 1;
    public final int VIEW_PROG = 0;

    public static View imgprog;
    public static View progressprog;

    public int visibleThreshold = 2;
    public int lastVisibleItem, totalItemCount;
    public static boolean loading;
    public static OnLoadMoreListener onLoadMoreListener;

    List<Person> listPerson;

    private Context context;

    public static int flagprog = 0;

    public static FragmentStruktural fragment;

    public Activity activity;
    public ListStrukturalAdapter(Context context, ArrayList<Person> listPerson, RecyclerView recyclerView, FragmentStruktural fragment, Activity activity) {
        super();

        this.fragment = fragment;
        this.context = context;
        this.activity = activity;
        this.listPerson = listPerson;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listPerson.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        Log.d("Progress",String.valueOf(viewType));
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.guru_item, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

        Log.d("flagdiadapter",String.valueOf(flagprog));
        if (viewHolder instanceof ViewHolder) {

            flagprog = 1;
            Person modelperson = listPerson.get(i);
            ((ViewHolder) viewHolder).txtnama.setText(modelperson.getName());
            ((ViewHolder) viewHolder).txtjabatan.setText(modelperson.getType());

            Picasso.with(context).load(BuildConfig.BASE_API_URL + "/" + modelperson.getPhoto().replace("\\", "/"))
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(((ViewHolder) viewHolder).imgThumbnail);

            //getdata
            ((ViewHolder) viewHolder).currentItem = listPerson.get(i);
        }
        else {

            flagprog = 0;
            Log.d("koneksi", String.valueOf(fragment.flagkoneksi));
            if(fragment.flagkoneksi == 1) {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.VISIBLE);
            }
            else
            {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.GONE);
            }

        }
    }

    public void addItem(Person person, int index) {
        listPerson.add(person);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        listPerson.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return listPerson.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setProgressOffSpan() {
        flagprog = 1;
    }

    public void setProgressOnSpan() {
        flagprog = 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView txtnama, txtjabatan;

        public Person currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.gambar_card);
            txtnama = (TextView)itemView.findViewById(R.id.nama);
            txtjabatan = (TextView)itemView.findViewById(R.id.jabatan);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(view.getContext(), DetilGuruActivity.class);

                    Bundle b = new Bundle();
                    b.putString("id_pegawai", String.valueOf(currentItem.getId()));
                    b.putString("nama_pegawai", String.valueOf(currentItem.getName()));
                    b.putString("jabatan", String.valueOf(currentItem.getDepartement()));
                    b.putString("telp", String.valueOf(currentItem.getPhone()));
                    b.putString("email", String.valueOf(currentItem.getEmail()));
                    b.putString("pendidikan", String.valueOf(currentItem.getEducation()));
                    b.putString("nip", String.valueOf(currentItem.getNip()));
                    b.putString("tipe", String.valueOf(currentItem.getType()));
                    b.putString("alamat", String.valueOf(currentItem.getAddress()));
                    b.putString("photo", String.valueOf(currentItem.getPhoto()));
                    b.putString("tgl_create", String.valueOf(currentItem.getCreated_at()));

                    i.putExtras(b);

                    ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(activity, imgThumbnail, "detil" );

                    ActivityCompat.startActivity(activity, i, options.toBundle());
                    //context.startActivity(i);

                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ImageView img;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
            img = (ImageView) v.findViewById(R.id.img);

            imgprog = img;
            progressprog = progressBar;

            img.setVisibility(View.GONE);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    fragment.deleteItem();
                    loading = true;
                }
            });
        }
    }
}


