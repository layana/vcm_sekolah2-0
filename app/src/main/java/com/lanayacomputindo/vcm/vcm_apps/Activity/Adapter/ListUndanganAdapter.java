package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilUndanganActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Invitation;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListUndanganAdapter extends RecyclerView.Adapter<ListUndanganAdapter.ViewHolder> {

    List<Invitation> listPost;

    private Context context;

    public Activity activity;
    public ListUndanganAdapter(Context context, ArrayList<Invitation> listPost, Activity activity) {
        super();

        this.context = context;
        this.activity = activity;
        this.listPost= listPost;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.undangan_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Invitation modelpost = listPost.get(i);
        viewHolder.txtjudul.setText(modelpost.getPost().getTitle());
        viewHolder.txttanggal.setText(modelpost.getPost().getCreated_at());

        //getdata
        viewHolder.currentItem = listPost.get(i);

    }

    @Override
    public int getItemCount() {
        return listPost.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtjudul, txttanggal;

        public Invitation currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtjudul = (TextView)itemView.findViewById(R.id.judul);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(view.getContext(), DetilUndanganActivity.class);

                        Bundle b = new Bundle();
                        b.putString("id_undangan", String.valueOf(currentItem.getId()));
                        b.putString("judul", String.valueOf(currentItem.getPost().getTitle()));
                        b.putString("deskripsi", String.valueOf(currentItem.getPost().getContent()));
                        b.putBoolean("status", currentItem.getValue());
                        b.putString("tgl_create", String.valueOf(currentItem.getCreated_at()));

                        i.putExtras(b);


                    ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(activity, txtjudul, "detil" );

                    ActivityCompat.startActivity(activity, i, options.toBundle());

                }
            });
        }

    }
}