package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class User {
    private Integer id;
    private Integer client_id;
    private String name;
    private String email;
    private String password;
    private String password_new;
    private String birthday;
    private String religion;
    private String phone;
    private String address;
    private String photo;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private String type;
    private Client client;
    private List<Device> device = new ArrayList<Device>();
    private List<Branch> branch = new ArrayList<Branch>();
    private List<PersonUser> person_user = new ArrayList<PersonUser>();
    private List<Post> post = new ArrayList<Post>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The client_id
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     *
     * @param client_id
     * The client_id
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password_new
     * The password_new
     */
    public void setPassword_new(String password_new) {
        this.password_new = password_new;
    }

    /**
     *
     * @return
     * The password_new
     */
    public String getPassword_new() {
        return password_new;
    }

    /**
     *
     * @return
     * The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     *
     * @param birthday
     * The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     *
     * @return
     * The religion
     */
    public String getReligion() {
        return religion;
    }

    /**
     *
     * @param religion
     * The religion
     */
    public void setReligion(String religion) {
        this.religion = religion;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The client
     */
    public Client getClient() {
        return client;
    }

    /**
     *
     * @param client
     * The client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     *
     * @return
     * The device
     */
    public List<Device> getDevice() {
        return device;
    }

    /**
     *
     * @param device
     * The device
     */
    public void setDevice(List<Device> device) {
        this.device = device;
    }

    /**
     *
     * @return
     * The branch
     */
    public List<Branch> getBranch() {
        return branch;
    }

    /**
     *
     * @param branch
     * The branch
     */
    public void setBranch(List<Branch> branch) {
        this.branch = branch;
    }

    /**
     *
     * @return
     * The person_user
     */
    public List<PersonUser> getPerson_user() {
        return person_user;
    }

    /**
     *
     * @param person_user
     * The person_user
     */
    public void setPerson_user(List<PersonUser> person_user) {
        this.person_user = person_user;
    }

    /**
     *
     * @return
     * The post
     */
    public List<Post> getPost() {
        return post;
    }

    /**
     *
     * @param post
     * The post
     */
    public void setPost(List<Post> post) {
        this.post = post;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
