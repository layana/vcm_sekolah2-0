package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilSurveyActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Survey;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListSurveyAdapter extends RecyclerView.Adapter<ListSurveyAdapter.ViewHolder> {

    List<Survey> listSurvey;

    private Context context;

    public ListSurveyAdapter(Context context, ArrayList<Survey> survey) {
        super();

        this.context = context;

        listSurvey = survey;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.survey_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Survey modelsurvey = listSurvey.get(i);
        viewHolder.txtjudul.setText(modelsurvey.getTitle());
        viewHolder.txttanggal.setText(modelsurvey.getCreated_at());
        if(modelsurvey.getStatus()) {
            viewHolder.txtstatus.setText("Status : Aktif");
        }
        else {
            viewHolder.txtstatus.setText("Status : Tidak Aktif");
        }

        if(modelsurvey.getCompleted()==0) {
            viewHolder.imgcek.setVisibility(View.GONE);
        }
        else {
            viewHolder.imgcek.setVisibility(View.VISIBLE);
        }

        //getdata
        viewHolder.currentItem = listSurvey.get(i);

    }



    @Override
    public int getItemCount() {
        return listSurvey.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtjudul, txttanggal, txtstatus;

        public Survey currentItem;

        public ImageView imgcek;

        public ViewHolder(View itemView) {
            super(itemView);
            txtjudul = (TextView)itemView.findViewById(R.id.judul);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);
            txtstatus = (TextView)itemView.findViewById(R.id.status);
            imgcek = (ImageView)itemView.findViewById(R.id.cek);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences sharedPreferences = context.getSharedPreferences("shared", Activity.MODE_PRIVATE);
                    int id = sharedPreferences.getInt("id", 0);


                    if(currentItem.getCompleted()==0) {
                        Intent i = new Intent(view.getContext(), DetilSurveyActivity.class);

                        Bundle b = new Bundle();
                        b.putString("id_survey", String.valueOf(currentItem.getId()));
                        b.putString("judul", String.valueOf(currentItem.getTitle()));
                        b.putString("link_url", String.valueOf(currentItem.getUrl()+"?user_id="+String.valueOf(id)));
                        b.putString("tgl_create", String.valueOf(currentItem.getCreated_at()));
                        b.putString("status", String.valueOf(currentItem.getStatus()));

                        i.putExtras(b);

                        context.startActivity(i);
                    }
                    else
                    {
                        //Toast.makeText(context,"Anda sudah mengikuti Survey ini", Toast.LENGTH_LONG).show();
                    }

                }
            });
        }

    }
}