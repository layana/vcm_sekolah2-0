package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Branch {
    private Integer id;
    private Integer client_id;
    private Integer user_id;
    private String email;
    private String name;
    private String phone;
    private String address;
    private String type;
    private String latitude;
    private String longitude;
    private String description;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Client client;
    private User user;
    private List<Person> person = new ArrayList<Person>();
    private List<ClassRoom> class_room = new ArrayList<ClassRoom>();
    private List<Post> post = new ArrayList<Post>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The client_id
     */
    public Integer getClient_id() {
        return client_id;
    }

    /**
     *
     * @param client_id
     * The client_id
     */
    public void setClient_id(Integer client_id) {
        this.client_id = client_id;
    }

    /**
     *
     * @return
     * The user_id
     */
    public Integer getUser_id() {
        return user_id;
    }

    /**
     *
     * @param user_id
     * The user_id
     */
    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The client
     */
    public Client getClient() {
        return client;
    }

    /**
     *
     * @param client
     * The client
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     *
     * @return
     * The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The person
     */
    public List<Person> getPerson() {
        return person;
    }

    /**
     *
     * @param person
     * The person
     */
    public void setPerson(List<Person> person) {
        this.person = person;
    }

    /**
     *
     * @return
     * The class_room
     */
    public List<ClassRoom> getClass_room() {
        return class_room;
    }

    /**
     *
     * @param class_room
     * The class_room
     */
    public void setClass_room(List<ClassRoom> class_room) {
        this.class_room = class_room;
    }

    /**
     *
     * @return
     * The post
     */
    public List<Post> getPost() {
        return post;
    }

    /**
     *
     * @param post
     * The post
     */
    public void setPost(List<Post> post) {
        this.post = post;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
