package com.lanayacomputindo.vcm.vcm_apps.Activity.Rest;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.ClassRoom;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Error;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Invitation;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.PersonUser;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Subject;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.SubjectTeacher;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Survey;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Teach;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Token;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Update;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Value;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;

import java.io.IOException;
import java.nio.charset.Charset;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class RestClient {

    private static Context mContext;
    private static Toast toast;
    private static GitApiInterface gitApiInterface ;
    private static String baseUrl = "http://sekolahkita.co" ;

    public static Retrofit retrofit() {
        OkHttpClient client = httpClient.build();
        return builder.client(client).build();
    }

    public static GitApiInterface getClient() {
        if (gitApiInterface == null) {

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

            Retrofit client = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            gitApiInterface = client.create(GitApiInterface.class);
        }
        return gitApiInterface ;
    }

    private static SharedPreferences sharedPref;
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Gson gson =
            new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BuildConfig.BASE_API_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson));

    public static GitApiInterface getClient(Context context) {
        if(context!=null) {
            mContext = context;
            sharedPref = mContext.getSharedPreferences("shared", Activity.MODE_PRIVATE);
            httpClient.interceptors().add(contentType);
            httpClient.interceptors().add(authentication);
            httpClient.interceptors().add(logging);
        }
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(GitApiInterface.class);
    }

    private static final Interceptor contentType = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authenticationRequest = originalRequest.newBuilder()
                    .header("Content-Type", "application/javascript")
                    .build();

            Response response = chain.proceed(authenticationRequest);

            return response;
        }
    };

    private static final Interceptor authentication = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Request authenticationRequest = originalRequest.newBuilder()
                    .header("Authorization", "Bearer " + sharedPref.getString("user_token", ""))
                    .build();

            Response response = chain.proceed(authenticationRequest);

            return response;
        }
    };

    private static final Interceptor logging = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {

            Request request = chain.request();
            final Response response = chain.proceed(request);

            ResponseBody responseBody = response.body();
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE); // Buffer the entire body.
            Buffer buffer = source.buffer();
            final String responseBodyString = buffer.clone().readString(Charset.forName("UTF-8"));

            if (response.code()!=200) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d("onFailure Body", responseBodyString);
                            Error error = gson.fromJson(responseBodyString, Error.class);
                            if(error.getMessage().equalsIgnoreCase("token_expired"))
                            {
                                showAToast("Sesi Anda telah habis atau Anda tidak memiliki akses pada menu ini. Silahkan logout terlebih dahulu");
                            }
                            //showAToast(error.getMessage());
                        } catch (Exception e) {

                        }
                    }
                });
            }

            return response;
        }
    };

    private static void showAToast (String st){ //"Toast toast" is declared in the class
        try{ toast.getView().isShown();     // true if visible
            toast.setText(st);
        } catch (Exception e) {         // invisible if exception
            toast = Toast.makeText(mContext, st, Toast.LENGTH_LONG);
        }
        toast.show();  //finally display it
    }


    public interface GitApiInterface {

        @FormUrlEncoded
        @POST("/api/v1/auth")
        Call<Result<Token>> getToken(@Field("client_id") int client_id, @Field("email") String  email, @Field("password") String  password);

        @FormUrlEncoded
        @POST("/api/v1/user/register")
        Call<Result<User>> register(@Field("client_id") int client_id,
                                    @Field("email") String  email,
                                    @Field("password") String  password,
                                    @Field("name") String  name,
                                    @Field("phone") String phone,
                                    @Field("address") String address,
                                    @Field("birthday") String birthday,
                                    @Field("religion") String religion,
                                    @Field("type") String type);

        @GET("/api/v1/user/myProfile")
        Call<Result<User>> getProfil();

        @FormUrlEncoded
        @PUT("/api/v1/user/updateProfile")
        Call<Result<User>> updateProfil(@Field("name") String name,@Field("phone") String phone,@Field("address") String address,@Field("birthday") String birthday,@Field("religion") String religion, @Field("photo") String photo);

        @FormUrlEncoded
        @PUT("/api/v1/user/updatePassword")
        Call<Result<User>> updatePassword(@Field("password") String password,@Field("password_new") String password_new);

        @FormUrlEncoded
        @POST("/api/v1/forgotPassword")
        Call<Result<User>> forgotPassword(@Field("email") String email);

        @FormUrlEncoded
        @POST("/api/v1/update/check")
        Call<Result<Update>> checkDevice(@Field("uuid") String uuid, @Field("version_code") String  version_code, @Field("name") String  name, @Field("model") String  model, @Field("platform") String platform, @Field("platform_version") String platform_version, @Field("gcm") String gcm);


        @GET("/api/v1/post")
        Call<Results<Post>> getPostWithOffset(@Query("search") String search, @Query("searchFields") String searchFields, @Query("with") String with, @Query("offset") String offset, @Query("limit") String limit);

        @GET("/api/v1/post")
        Call<Results<Post>> getPost(@Query("search") String search, @Query("searchFields") String searchFields, @Query("with") String with);

        @GET("/api/v1/attendance/{person_id}")
        Call<Results<Post>> getAbsen(@Path("person_id") String person_id);

        @GET("/api/v1/person")
        Call<Results<Person>> getPersonWithOffset(@Query("search") String search, @Query("searchFields") String searchFields,  @Query("offset") String offset, @Query("limit") String limit);

        @GET("/api/v1/person")
        Call<Results<Person>> getPerson(@Query("search") String search, @Query("searchFields") String searchFields);

        @GET("/api/v1/person/myChild")
        Call<Results<Person>> getAnakku(@Query("with") String with);

        @GET("/api/v1/branch/clientBranch")
        Call<Results<Branch>> getClientBranch();

        @GET("/api/v1/branch/myBranch")
        Call<Results<Branch>> getUserBranch();

        @GET("/api/v1/class_room")
        Call<Results<ClassRoom>> getClassRoom(@Query("search") String search);

        @FormUrlEncoded
        @POST("/api/v1/person/myChild")
        Call<Result<Person>> addChild(@Field("name") String name, @Field("nip") String nip, @Field("birthday") String birthday, @Field("photo") String photo, @Field("branch_id") int branchId, @Field("class_room_id") int classRoomId);

        @FormUrlEncoded
        @PUT("/api/v1/person/myChild/{id}")
        Call<Result<Person>> updateChild(@Path("id") int id, @Field("name") String name, @Field("nip") String nip, @Field("birthday") String birthday, @Field("photo") String photo, @Field("branch_id") int branchId, @Field("class_room_id") int classRoomId);

        @GET("/api/v1/invitation/myInvitation")
        Call<Results<Invitation>> getIntivation(@Query("with") String with, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);

        @FormUrlEncoded
        @PUT("/api/v1/invitation/myInvitation/{id}")
        Call<Result<Invitation>> updateInvitation(@Path("id") int id, @Field("value") String value);

        @GET("/api/v1/announcement/my")
        Call<Results<Post>> getMyNews(@Query("with") String with, @Query("offset") String offset, @Query("limit") String limit, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);

        @GET("/api/v1/announcement/class")
        Call<Results<Post>> getClassNews(@Query("with") String with, @Query("offset") String offset, @Query("limit") String limit, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);

        @GET("/api/v1/announcement/branch/{id}")
        Call<Results<Post>> getBranchNews(@Path("id") int id, @Query("with") String with, @Query("offset") String offset, @Query("limit") String limit, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);

        @GET("/api/v1/announcement/branch/{id}")
        Call<Results<Post>> getBranchNewsDashboard(@Path("id") int id, @Query("with") String with, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy, @Query("limit") String limit);

        @GET("/api/v1/survey")
        Call<Results<Survey>> getSurvey();

        @GET("/api/v1/subject")
        Call<Results<Subject>> getSubject(@Query("search") String search);

        @GET("/api/v1/teach")
        Call<Results<Teach>> getTeachOnline(@Query("with") String with, @Query("search") String search);

        @GET("/api/v1/value")
        Call<Results<Value>> getValue(@Query("with") String with, @Query("search") String search, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);

        @GET("/api/v1/subject_teacher")
        Call<Results<SubjectTeacher>> getSubjectTeacherToday(@Query("with") String with, @Query("search") String search, @Query("orderBy") String orderBy, @Query("sortedBy") String sortedBy);


        @FormUrlEncoded
        @POST("/api/v1/post/store")
        Call<Result<Post>> postPengumuman(@Field("type") String type, @Field("title") String  title, @Field("content") String  content, @Field("status") Boolean status, @Field("broadcast_type") String broadcast_type, @Field("photo") String photo, @Field("class_room_id") String class_room_id, @Field("parent_id") String parent_id);

        @GET("/api/v1/classRoom")
        Call<Results<ClassRoom>> getClassBranch(@Query("type") String type);

        @GET("/api/v1/personUser")
        Call<Results<PersonUser>> getWaliMurid(@Query("with") String with);

        @FormUrlEncoded
        @POST("/api/v1/post/store")
        Call<Result<Post>> postUndangan(@Field("type") String type, @Field("title") String  title, @Field("content") String  content, @Field("status") Boolean status, @Field("broadcast_type") String broadcast_type, @Field("start_date") String start_date, @Field("end_date") String end_date, @Field("class_room_id") String class_room_id, @Field("parent_id") String parent_id);

    }

}
