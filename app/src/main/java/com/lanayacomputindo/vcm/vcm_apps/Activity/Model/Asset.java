package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Asset {
    private Integer id;
    private Integer post_id;
    private String type;
    private String name;
    private String content;
    private String file;
    private Integer status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Post post;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The post_id
     */
    public Integer getPost_id() {
        return post_id;
    }

    /**
     *
     * @param post_id
     * The post_id
     */
    public void setPost_id(Integer post_id) {
        this.post_id = post_id;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The content
     */
    public String getContent() {
        return content;
    }

    /**
     *
     * @param content
     * The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     *
     * @return
     * The file
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The post
     */
    public Post getPost() {
        return post;
    }

    /**
     *
     * @param post
     * The post
     */
    public void setPost(Post post) {
        this.post = post;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
