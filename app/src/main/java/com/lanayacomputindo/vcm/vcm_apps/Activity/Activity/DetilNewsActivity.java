package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.TimeFormater;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class DetilNewsActivity extends AppCompatActivity {

    private Intent shareIntent;

    private String bundleid, bundlejudul, bundleisi, bundlecreate, bundlephoto, bundlemerchant;

    private TextView txtisi, txtcreate, txtjudul;
    private ImageView imgphoto;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("idnews");
        bundlejudul = b.getString("judul");
        bundleisi = b.getString("isi");
        bundlecreate = b.getString("create");
        bundlephoto = b.getString("photo");
        bundlemerchant = b.getString("merchant");


        getSupportActionBar().setTitle(bundlejudul);
        txtisi = (TextView) findViewById(R.id.isi);
        txtcreate = (TextView) findViewById(R.id.create);
        txtjudul  = (TextView) findViewById(R.id.judul);
        imgphoto = (ImageView) findViewById(R.id.photo);
        txtisi.setText(Html.fromHtml(bundleisi));
        //txtcreate.setText(bundlecreate);
        txtcreate.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd hh:mm:ss", "dd MMM yyyy", bundlecreate));
        txtjudul.setText(bundlejudul);

        if(bundlephoto!=null) {
            Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto.replace("\\", "/"))
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(imgphoto);
        }

        imgphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), DetilGambarActivity.class);

                Bundle b = new Bundle();

                b.putString("photo", bundlephoto);
                b.putString("namaproduk", bundlejudul);
                b.putString("param", "berita");

                i.putExtras(b);

                ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(DetilNewsActivity.this, imgphoto, "detil" );
                //Start the Intent
                ActivityCompat.startActivity(DetilNewsActivity.this, i, options.toBundle());

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void setupShareIntent() {
        Uri bmpUri = getLocalBitmapUri(imgphoto);
        shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, bundlejudul);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(Intent.createChooser(shareIntent, "Share ke media sosial . ."));
    }

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable){
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detil_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                DetilNewsActivity.this.supportFinishAfterTransition();

                return true;

            case R.id.menu_item_share:

                setupShareIntent();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
