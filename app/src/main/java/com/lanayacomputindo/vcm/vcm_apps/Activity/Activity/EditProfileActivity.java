package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    private EditText edalamat, ednama, ednotelp, edlahir;

    private Spinner spagama;

    private TextView txtemail;

    private Button btnsimpan, btnedpassword;

    private CircleImageView imgphoto;

    File fileimage = null;

    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;

    private  ProgressDialog pDialog;

    Bitmap imageupload;

    private String imageprofil, imageqrcode;
    //File sourceFile;

    protected static final int DATE_DIALOG_ID = 0;

    private Date date2 = new Date();
    private String tanggal;

    String[] permissioncamera = new String[]{Manifest.permission.CAMERA};

    int id,client_id;
    boolean status;
    String name,email,birthday,religion,phone,address,photo,created_at,updated_at;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            id = sharedPreferences.getInt("id", 0);
            client_id = sharedPreferences.getInt("client_id", 0);
            name = sharedPreferences.getString("name", "");
            email = sharedPreferences.getString("email", "");
            birthday = sharedPreferences.getString("birthday", "");
            religion = sharedPreferences.getString("religion", "");
            phone = sharedPreferences.getString("phone", "");
            address = sharedPreferences.getString("address", "");
            photo = sharedPreferences.getString("photo", "");
            status = sharedPreferences.getBoolean("status", false);
            created_at = sharedPreferences.getString("created_at", "");
            updated_at = sharedPreferences.getString("updated_at", "");
        } else {

        }
    }

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String name,
                                 String email,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 boolean status,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putBoolean("status", status);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.getBackground().setAlpha(95);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        loadPreferences();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        txtemail = (TextView) findViewById(R.id.email);
        ednama = (EditText) findViewById(R.id.nama);
        edalamat = (EditText) findViewById(R.id.alamat);
        ednotelp = (EditText) findViewById(R.id.notelp);
        imgphoto = (CircleImageView) findViewById(R.id.photo);
        edlahir = (EditText) findViewById(R.id.lahir);
        spagama = (Spinner) findViewById(R.id.agama);

        edlahir.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edlahir)
                    showDialog(DATE_DIALOG_ID);
                return false;
            }
        });

        btnsimpan = (Button) findViewById(R.id.btnsimpan);
        btnedpassword = (Button) findViewById(R.id.btnedpassword);

        DateFormat formatter = new SimpleDateFormat("yyy-MM-dd");
        try {

            date2 = formatter.parse(birthday);
        }
        catch(ParseException ex){

        }

        txtemail.setText(email);
        ednama.setText(name);
        edalamat.setText(address);
        ednotelp.setText(phone);
        edlahir.setText(birthday);

        spagama.setSelection(getIndex(spagama, religion));

        String photoo = photo.replace("\\", "/");
        Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + photoo)
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(imgphoto);

        imageupload = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

        imgphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckPermission
                        .from(EditProfileActivity.this)
                        .setPermissions(permissioncamera)
                        .setRationaleConfirmText("Meminta ijin mengakses kamera")
                        .setDeniedMsg("The Camera Denied")
                        .setPermissionListener(new PermissionListener() {

                            @Override
                            public void permissionGranted() {
                                selectImage();
                            }

                            @Override
                            public void permissionDenied() {
                                Toast.makeText(getApplicationContext(), "Mengakses kamera tidak diijinkan", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .check();


            }
        });

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfile();
            }
        });

        btnedpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent l = new Intent(getApplicationContext(),EditPasswordActivity.class);
                startActivity(l);
            }
        });

    }

    private int getIndex(Spinner spinner, String myString)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void editProfile()
    {
        pDialog = ProgressDialog.show(EditProfileActivity.this,
                "Mengedit Profil Anda",
                "Tunggu Sebentar!");

        String image = getStringImage(imageupload);
        RestClient.GitApiInterface service = RestClient.getClient(this);

        Call<Result<User>> call = service.updateProfil(ednama.getText().toString(), ednotelp.getText().toString(), edalamat.getText().toString(), tanggal, spagama.getSelectedItem().toString(), image);

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call, Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());

                        pDialog.dismiss();
                        finish();
                    }  else {
                        Log.e("Update Profil", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Update Profil", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Log.d("gagal", "agagagagal");
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = { "Ambil Foto", "Pilih dari Gallery",
                "Batal" };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Tambah Gambar!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {

                    fileimage = new File(Environment.getExternalStorageDirectory(),
                            "kartuvirtual.jpg");

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileimage));
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if (items[item].equals("Pilih dari Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            if (requestCode == SELECT_FILE) {
                imageUri = CropImage.getPickImageResultUri(this, data);
            }
            else if (requestCode == REQUEST_CAMERA) {
//                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        "kartuvirtual.jpg");
//
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                imageUri = Uri.fromFile(fileimage);
            }

            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                imgphoto.setImageURI(result.getUri());

                Bitmap bitmap = ((BitmapDrawable)imgphoto.getDrawable()).getBitmap();

                imageupload = bitmap;

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        c.setTime(date2);
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            tanggal = date_selected;
            edlahir.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+String.valueOf(year));
        }
    };



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                EditProfileActivity.this.finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
