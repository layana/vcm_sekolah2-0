package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListAbsenAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListKalenderAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Decorator.EventDecorator;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.DBHandler;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentAbsen extends Fragment implements OnDateSelectedListener, OnMonthChangedListener {

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    MaterialCalendarView widget;

    ArrayList<Post> listKalender = new ArrayList<Post>();
    ArrayList<Post> listKalenderEvent = new ArrayList<Post>();

    ArrayList<String> listColor = new ArrayList<String>();
    ProgressDialog pDialog;

    DBHandler db;

    int branch_id;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            branch_id = sharedPreferences.getInt("branch_id", 0);
        } else {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        db = new DBHandler(getActivity());
        loadPreferences();
        View v = inflater.inflate(R.layout.kalender_akademik, container, false);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        widget = (MaterialCalendarView) v.findViewById(R.id.calendarView);

        widget.setOnDateChangedListener(this);
        widget.setOnMonthChangedListener(this);
        widget.setSelectedDate(Calendar.getInstance());

        getAbsen();

        return v;
    }

    public void getAbsen()
    {
        pDialog = ProgressDialog.show(getActivity(),
                "Mengambil data Kalender Akademik",
                "Tunggu Sebentar!");

        RestClient.GitApiInterface service = RestClient.getClient(getActivity());

        Call<Results<Post>>call = service.getAbsen(String.valueOf(sharedPreferences.getInt("anak_id", 0)));

        call.enqueue(new Callback<Results<Post>>() {
            @Override
            public void onResponse(Call<Results<Post>> call,  Response<Results<Post>> response) {
                pDialog.dismiss();
                Log.d("kalender", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Post> result = response.body();
                    Log.d("kalender", "response = " + new Gson().toJson(result));

                    listKalender = result.getData();

                    db.deleteAllKalender();
                    for (int i = 0; i< listKalender.size(); i++) {
                        String startDateString = listKalender.get(i).getStart_date();
                        String endDateString = listKalender.get(i).getEnd_date();

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                        Date startDate = null;
                        Date endDate = null;

                        Calendar start = Calendar.getInstance();
                        Calendar end = Calendar.getInstance();

                        try {
                            startDate = df.parse(startDateString);
                            endDate = df.parse(endDateString);

                            String newStartDateString = df.format(startDate);
                            String newEndDateString = df.format(endDate);

                            start.setTime(startDate);
                            end.setTime(endDate);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        long selisih = end.getTimeInMillis() - start.getTimeInMillis();
                        long hari = selisih / (24 * 60 * 60 * 1000);
                        for (int j = 0; j <= hari; j++) {

                            String yyyy = String.valueOf(start.get(Calendar.YEAR));
                            String MM = String.valueOf(start.get(Calendar.MONTH) + 1);
                            if(Integer.valueOf(MM) < 10)
                            {
                                MM = "0"+MM;
                            }
                            String dd = String.valueOf(start.get(Calendar.DAY_OF_MONTH));
                            if(Integer.valueOf(dd) < 10)
                            {
                                dd = "0"+dd;
                            }

                            db.insertKalender(String.valueOf(listKalender.get(i).getId()),
                                    listKalender.get(i).getTitle(),
                                    listKalender.get(i).getContent(),
                                    yyyy+"-"+MM+"-"+dd,
                                    String.valueOf(listKalender.get(i).getStatus()));

                            //Log.d("tanggalan", yyyy+"-"+MM+"-"+dd);
                            start.add(Calendar.DATE, 1);
                        }

                    }

                    DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
                    CalendarDay datee = widget.getSelectedDate();

                    listKalenderEvent = db.getKalenderByDate(FORMATTER.format(datee.getDate()));
                    mAdapter = new ListKalenderAdapter(getActivity(), listKalenderEvent);
                    mRecyclerView.setAdapter(mAdapter);

                    new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Post>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("kalender", t.getLocalizedMessage());
                Log.d("gagal", "agagagagal");
                Toast.makeText(getActivity(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });

    }

    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {

            ArrayList<CalendarDay> dates = new ArrayList<>();
            for (int i = 0; i < listKalender.size(); i++) {
                String startDateString = listKalender.get(i).getStart_date();
                String endDateString = listKalender.get(i).getEnd_date();

                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date startDate = null;
                Date endDate = null;

                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();

                try {
                    startDate = df.parse(startDateString);
                    endDate = df.parse(endDateString);
                    start.setTime(startDate);
                    end.setTime(endDate);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long selisih = end.getTimeInMillis() - start.getTimeInMillis();
                long hari = selisih / (24 * 60 * 60 * 1000);

                //Log.d("hari", String.valueOf(hari));

                for (int j = 0; j <= hari; j++) {
                    CalendarDay day = CalendarDay.from(start);
                    dates.add(day);
                    start.add(Calendar.DATE, 1);
                    listColor.add(listKalender.get(i).getColor());
                }



            }
            return dates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);
            ArrayList<CalendarDay> kuning = new ArrayList<CalendarDay>();
            ArrayList<CalendarDay> merah = new ArrayList<CalendarDay>();
            ArrayList<CalendarDay> hijau = new ArrayList<CalendarDay>();
            for(int i = 0; i < calendarDays.size(); i++)
            {
                if(listColor.get(i).equalsIgnoreCase("merah"))
                {
                    merah.add(calendarDays.get(i));
                }
                if(listColor.get(i).equalsIgnoreCase("hijau"))
                {
                    hijau.add(calendarDays.get(i));
                }
                if(listColor.get(i).equalsIgnoreCase("kuning"))
                {
                    kuning.add(calendarDays.get(i));
                }
            }
            widget.addDecorator(new EventDecorator(Color.BLACK, kuning, ContextCompat.getDrawable(getActivity(), R.drawable.kuning)));
            widget.addDecorator(new EventDecorator(Color.BLACK, hijau, ContextCompat.getDrawable(getActivity(), R.drawable.hijau)));
            widget.addDecorator(new EventDecorator(Color.BLACK, merah, ContextCompat.getDrawable(getActivity(), R.drawable.merah)));

            WhiteTextDayDecorator selected = new WhiteTextDayDecorator(calendarDays);
            widget.addDecorator(selected);

            SundayDecorator sunday = new SundayDecorator();
            widget.addDecorator(sunday);
        }
    }


    public class WhiteTextDayDecorator implements DayViewDecorator {

        private HashSet<CalendarDay> dates;

        public WhiteTextDayDecorator(Collection<CalendarDay> dates) {
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(Color.WHITE));
        }
    }

    public class SundayDecorator implements DayViewDecorator {
        public SundayDecorator() {
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return day.getCalendar().get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new ForegroundColorSpan(Color.parseColor("#F44336")));
        }
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @Nullable CalendarDay date, boolean selected) {
        //textView.setText(getSelectedDatesString());
        DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

        Log.d("select", String.valueOf(FORMATTER.format(date.getDate())));
        listKalenderEvent = db.getKalenderByDate(FORMATTER.format(date.getDate()));
        mAdapter = new ListAbsenAdapter(getActivity(), listKalenderEvent);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
        //noinspection ConstantConditions
        //getActivity().getSupportActionBar().setTitle(FORMATTER.format(date.getDate()));
    }


}
