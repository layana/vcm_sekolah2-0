package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.NavigasiActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListTokoMercantAdapter extends RecyclerView.Adapter<ListTokoMercantAdapter.ViewHolder> {

    List<Branch> listtoko;

    private Context context;

    ArrayList<String> listDuration = new ArrayList<String>();
    ArrayList<String> listDistance = new ArrayList<String>();

    public ListTokoMercantAdapter(Context context,  ArrayList<Branch> merchant, ArrayList<String> duration, ArrayList<String> distance) {
        super();

        listDuration = duration;
        listDistance = distance;

        listDuration.add("aaa");
        listDuration.add("aaa");
        listDistance.add("aaa");
        listDistance.add("aaa");

        this.context = context;

        listtoko = merchant;

        Log.d("Size List", String.valueOf(listtoko.size()));

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.tokomerchant_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Branch modelBranch = listtoko.get(i);
        String duration = listDuration.get(i);
        String distance = listDistance.get(i);
        viewHolder.txtnama.setText(modelBranch.getName());
        viewHolder.txtwaktu.setText(duration);
        viewHolder.txtalamat.setText(modelBranch.getAddress());
        viewHolder.txtjarak.setText(distance);

        viewHolder.currentItem = listtoko.get(i);

    }



    @Override
    public int getItemCount() {
        return listtoko.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView txtnama, txtwaktu, txtjarak, txtalamat;

        public Branch currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            txtnama = (TextView)itemView.findViewById(R.id.namacabang);
            txtwaktu = (TextView)itemView.findViewById(R.id.waktu);
            txtjarak = (TextView)itemView.findViewById(R.id.jarak);
            txtalamat = (TextView)itemView.findViewById(R.id.alamat);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    Intent i = new Intent(view.getContext(), TampilLIstTokoActivity.class);
//
//                    Bundle b = new Bundle();
//                    b.putString("wisata", String.valueOf(currentItem.getNama()));
//
//                    i.putExtras(b);
//
//                    context.startActivity(i);
                    LatLng posisitoko = new LatLng(Double.parseDouble(currentItem.getLatitude()), Double.parseDouble(currentItem.getLongitude()));

                    NavigasiActivity.moveCameraMap(posisitoko);

                }
            });
        }
    }
}


