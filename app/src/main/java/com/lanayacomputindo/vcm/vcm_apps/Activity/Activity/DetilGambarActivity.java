package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.widget.ImageView;

import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

public class DetilGambarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_gambar);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        String bundlenama = b.getString("namaproduk");
        String bundlephoto = b.getString("photo");

        getSupportActionBar().setTitle(bundlenama);

        ImageView imgphoto = (ImageView) findViewById(R.id.photo);

        if(bundlephoto!=null) {
            Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto.replace("\\", "/"))
                    .error(R.drawable.loadimage)
                    .placeholder(R.drawable.loadimage)
                    .into(imgphoto);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                supportFinishAfterTransition();

                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}
