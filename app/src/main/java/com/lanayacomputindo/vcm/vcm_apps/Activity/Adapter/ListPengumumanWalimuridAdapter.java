package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilNewsActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentPengumumanWalimurid;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.OnLoadMoreListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.TimeFormater;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListPengumumanWalimuridAdapter extends RecyclerView.Adapter {

    public final int VIEW_ITEM = 1;
    public final int VIEW_PROG = 0;

    public int visibleThreshold = 1;
    public int lastVisibleItem, totalItemCount;
    public static boolean loading;
    public static OnLoadMoreListener onLoadMoreListener;

    List<Post> listPost;

    private Context context;

    public static FragmentPengumumanWalimurid fragment;

    public Activity activity;

    public ListPengumumanWalimuridAdapter(Context context, ArrayList<Post> listPost, RecyclerView recyclerView, FragmentPengumumanWalimurid fragment, Activity activity) {
        super();

        this.fragment = fragment;

        this.context = context;

        this.activity = activity;

        this.listPost = listPost;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listPost.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        Log.d("Progress",String.valueOf(viewType));
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.news_item, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {

            ((ViewHolder) viewHolder).currentItem = listPost.get(i);
            Post modelpost = listPost.get(i);
            ((ViewHolder) viewHolder).txtnama.setText(modelpost.getTitle());
            ((ViewHolder) viewHolder).txtdesc.setText(Html.fromHtml(modelpost.getContent()));
            //((ViewHolder) viewHolder).txttanggal.setText(modelpost.getCreated_at());
            ((ViewHolder) viewHolder).txttanggal.setText(new TimeFormater().formattedDateFromString("", "", modelpost.getCreated_at()));

            try {
                Picasso.with(context).load(BuildConfig.BASE_API_URL + "/" + modelpost.getAsset().get(0).getFile().replace("\\", "/"))
                        .error(R.drawable.loadimage)
                        .placeholder(R.drawable.loadimage)
                        .into(((ViewHolder) viewHolder).imgThumbnail);
            }
            catch(Exception e)
            {

            }

            //getdata
            ((ViewHolder) viewHolder).currentItem = listPost.get(i);
        }
        else {
            if(fragment.flagkoneksi == 1) {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.VISIBLE);
            }
            else
            {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.GONE);
            }
        }
    }

    public void addItem(Post news, int index) {
        listPost.add(news);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        listPost.remove(index);
        notifyItemRemoved(index);
    }


    @Override
    public int getItemCount() {
        return listPost.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView txtnama, txtdesc, txttanggal;

        public Post currentItem;

        public ViewHolder(View itemView) {

            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.gambar_card);
            txtnama = (TextView)itemView.findViewById(R.id.nama);
            txtdesc = (TextView)itemView.findViewById(R.id.deskripsi);
            txttanggal = (TextView)itemView.findViewById(R.id.tanggal);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), DetilNewsActivity.class);

                    Bundle b = new Bundle();
                    b.putString("idnews", String.valueOf(currentItem.getId()));
                    b.putString("judul", String.valueOf(currentItem.getTitle()));
                    b.putString("isi", String.valueOf(currentItem.getContent()));
                    b.putString("create", String.valueOf(currentItem.getCreated_at()));
                    if(currentItem.getAsset().size()>0) {
                        b.putString("photo", String.valueOf(currentItem.getAsset().get(0).getFile()));
                    }
                    intent.putExtras(b);
                    ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(activity, imgThumbnail, "detil" );

                    ActivityCompat.startActivity(activity, intent, options.toBundle());
                    //context.startActivity(i);

                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ImageView img;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
            img = (ImageView) v.findViewById(R.id.img);

            img.setVisibility(View.GONE);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                    fragment.deleteItem();
                }
            });
        }
    }
}


