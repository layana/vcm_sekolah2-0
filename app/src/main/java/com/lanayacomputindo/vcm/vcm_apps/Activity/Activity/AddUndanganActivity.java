package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.captain_miao.grantap.CheckPermission;
import com.example.captain_miao.grantap.listeners.PermissionListener;
import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Branch;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.ClassRoom;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.PersonUser;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUndanganActivity extends AppCompatActivity {

    private Button btnkirim;

    private EditText edtitle, edisi, edstart, edend;

    private SearchableSpinner spKelas, spWali;

    private RadioGroup radiojenis;
    private RadioButton radiopilih;

    LinearLayout llclass, llprivate;

    private ProgressDialog pDialog;

    protected static final int start_id = 0, end_id = 1;

    private String broadcast = "all", class_room_id = "", parent = "", start_date = "", end_date = "";

    private RestClient.GitApiInterface service;

    private ArrayList<ClassRoom> listKelas = new ArrayList<ClassRoom>();
    private ArrayList<String> listNamakelas = new ArrayList<String>();
    private ArrayAdapter<String> adapterKelas;

    private ArrayList<User> listWali = new ArrayList<User>();
    private ArrayList<String> listNamaWali = new ArrayList<String>();
    private ArrayAdapter<String> adapterWali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_undangan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Tambah Undangan");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        service = RestClient.getClient(this);

        edtitle = (EditText) findViewById(R.id.title);
        edisi = (EditText) findViewById(R.id.isi);
        edstart = (EditText) findViewById(R.id.etstart);
        edend = (EditText) findViewById(R.id.etend);
        btnkirim = (Button) findViewById(R.id.btnkirim);

        spKelas = (SearchableSpinner) findViewById(R.id.spkelas);
        spWali = (SearchableSpinner) findViewById(R.id.spwalimurid);

        spKelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listKelas.size()>0) {
                    class_room_id = String.valueOf(listKelas.get(i).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spWali.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (listWali.size()>0) {
                    parent = String.valueOf(listWali.get(i).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btnkirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(broadcast.equalsIgnoreCase("all")) {
                    postUndangan();
                }
                if(broadcast.equalsIgnoreCase("class")) {
                    if(!class_room_id.equalsIgnoreCase("")) postUndangan();
                }
                if(broadcast.equalsIgnoreCase("private")) {
                    if(!parent.equalsIgnoreCase("")) postUndangan();
                }
            }
        });

        llclass = (LinearLayout) findViewById(R.id.llkelas);
        llprivate = (LinearLayout) findViewById(R.id.llwali);
        llclass.setVisibility(View.GONE);
        llprivate.setVisibility(View.GONE);

        radiojenis = (RadioGroup) findViewById(R.id.rg);
        radiojenis.check(R.id.rb_all);

        int selectedId = radiojenis.getCheckedRadioButtonId();

        radiopilih = (RadioButton) findViewById(selectedId);

        radiojenis.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectId = radiojenis.getCheckedRadioButtonId();

                radiopilih = (RadioButton) findViewById(selectId);

                if(radiopilih.getText().toString().equalsIgnoreCase("Semua"))
                {
                    broadcast = "all";
                    llclass.setVisibility(View.GONE);
                    llprivate.setVisibility(View.GONE);
                }
                else if(radiopilih.getText().toString().equalsIgnoreCase("Kelas"))
                {
                    broadcast = "class";
                    llclass.setVisibility(View.VISIBLE);
                    llprivate.setVisibility(View.GONE);
                }
                else if(radiopilih.getText().toString().equalsIgnoreCase("Walimurid"))
                {
                    broadcast = "private";
                    llclass.setVisibility(View.GONE);
                    llprivate.setVisibility(View.VISIBLE);
                }
            }
        });

        edstart.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edstart)
                    showDialog(start_id);
                return false;
            }
        });

        edend.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (v == edend)
                    showDialog(end_id);
                return false;
            }
        });

        getKelas();

    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        Calendar c = Calendar.getInstance();
        int cyear = c.get(Calendar.YEAR);
        int cmonth = c.get(Calendar.MONTH);
        int cday = c.get(Calendar.DAY_OF_MONTH);

        switch (id) {
            case start_id:
                return new DatePickerDialog(this, mDateSetListenerStart, cyear, cmonth, cday);
            case end_id:
                return new DatePickerDialog(this, mDateSetListenerEnd, cyear, cmonth, cday);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener mDateSetListenerStart = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            start_date = date_selected;
            edstart.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+String.valueOf(year));
        }
    };

    private DatePickerDialog.OnDateSetListener mDateSetListenerEnd = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            String date_selected = String.valueOf(year)+"-"
                    + String.valueOf(monthOfYear + 1)+ "-"
                    + String.valueOf(dayOfMonth);

            String bulan = "";
            if((monthOfYear+1)==1)
            {
                bulan = "Januari";
            }
            if((monthOfYear+1)==2)
            {
                bulan = "Februari";
            }
            if((monthOfYear+1)==3)
            {
                bulan = "Maret";
            }
            if((monthOfYear+1)==4)
            {
                bulan = "April";
            }
            if((monthOfYear+1)==5)
            {
                bulan = "Mei";
            }
            if((monthOfYear+1)==6)
            {
                bulan = "Juni";
            }
            if((monthOfYear+1)==7)
            {
                bulan = "Juli";
            }
            if((monthOfYear+1)==8)
            {
                bulan = "Agustus";
            }
            if((monthOfYear+1)==9)
            {
                bulan = "September";
            }
            if((monthOfYear+1)==10)
            {
                bulan = "Oktober";
            }
            if((monthOfYear+1)==11)
            {
                bulan = "November";
            }
            if((monthOfYear+1)==12)
            {
                bulan = "Desember";
            }

            end_date = date_selected;
            edend.setText(String.valueOf(dayOfMonth)+" "+bulan+" "+String.valueOf(year));
        }
    };


    public void getKelas()
    {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        pDialog = ProgressDialog.show(AddUndanganActivity.this,
                "",
                "Tunggu Sebentar!");

        Call<Results<ClassRoom>> call = service.getClassBranch("branch");
        call.enqueue(new Callback<Results<ClassRoom>>() {
            @Override
            public void onResponse(Call<Results<ClassRoom>> call, Response<Results<ClassRoom>> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        listKelas.clear();
                        listNamakelas.clear();

                        for (final ClassRoom classRoom : response.body().getData()) {
                            listKelas.add(classRoom);
                            listNamakelas.add(classRoom.getName());
                        }
                        adapterKelas = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.spinner_item, listNamakelas);

                        spKelas.setAdapter(adapterKelas);
                        getWaliMurid();
                    } else {
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e("Get Kelas" + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e("Get Kelas" + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<ClassRoom>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e("Get Kelas" + " failure", String.valueOf(t.getMessage().toString()));
            }
        });

    }

    public void getWaliMurid()
    {

        Call<Results<PersonUser>> call = service.getWaliMurid("user");
        call.enqueue(new Callback<Results<PersonUser>>() {
            @Override
            public void onResponse(Call<Results<PersonUser>> call, Response<Results<PersonUser>> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        listWali.clear();
                        listNamaWali.clear();

                        for (final PersonUser user : response.body().getData()) {
                            listWali.add(user.getUser());
                            listNamaWali.add(user.getUser().getName());
                        }
                        adapterWali = new ArrayAdapter<String>(getApplicationContext(),
                                R.layout.spinner_item, listNamaWali);

                        spWali.setAdapter(adapterWali);
                    } else {
                        Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        Log.e("Get Wali" + " response", String.valueOf(response.raw().toString()));
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Error: " + String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    Log.e("Get Wali" + " response", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Results<PersonUser>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Failure : " + String.valueOf(t.getMessage().toString()), Toast.LENGTH_SHORT).show();
                Log.e("Get Wali" + " failure", String.valueOf(t.getMessage().toString()));
            }
        });

    }


    public void postUndangan(){
        pDialog = ProgressDialog.show(AddUndanganActivity.this,
                "Mengirim Undangan",
                "Tunggu Sebentar!");

        RestClient.GitApiInterface service = RestClient.getClient(this);

        Log.d("isi post", edtitle.getText().toString() + edisi.getText().toString() + broadcast + class_room_id + parent);
        Call<Result<Post>> call = service.postUndangan("invitation",edtitle.getText().toString(), edisi.getText().toString(), true, broadcast, start_date, end_date, class_room_id, parent);

        call.enqueue(new Callback<Result<Post>>() {
            @Override
            public void onResponse(Call<Result<Post>> call, Response<Result<Post>> response) {
                Log.d("Post", "Status Code = " + response.code());
                pDialog.dismiss();
                if (response.isSuccessful()) {
                    Result<Post> result = response.body();
                    Log.d("Post", "response = " + new Gson().toJson(result));
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    finish();

                } else {
                    Log.e("Post", String.valueOf(response.raw().toString()));
                }
            }

            @Override
            public void onFailure(Call<Result<Post>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("Post", t.toString());
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
