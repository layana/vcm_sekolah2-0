package com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilExtraActivity;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment.FragmentExtra;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.OnLoadMoreListener;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Post;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Edwin on 18/01/2015.
 */

public class ListExtraAdapter extends RecyclerView.Adapter{

    public final int VIEW_ITEM = 1;
    public final int VIEW_PROG = 0;

    public static View imgprog;
    public static View progressprog;

    public int visibleThreshold = 2;
    public int lastVisibleItem, totalItemCount;
    public static boolean loading;
    public static OnLoadMoreListener onLoadMoreListener;

    List<Post> listPost;

    private Context context;

    public static int flagprog = 0;

    public static FragmentExtra fragment;

    public Activity activity;
    public ListExtraAdapter(Context context, ArrayList<Post> listPost, RecyclerView recyclerView, FragmentExtra fragment, Activity activity) {
        super();

        this.fragment = fragment;
        this.context = context;
        this.activity = activity;
        this.listPost= listPost;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager
                            .findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return listPost.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        Log.d("Progress",String.valueOf(viewType));
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.katalog_item, parent, false);

            vh = new ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

        Log.d("flagdiadapter",String.valueOf(flagprog));
        if (viewHolder instanceof ViewHolder) {

            flagprog = 1;
            Post modelpost = listPost.get(i);
            ((ViewHolder) viewHolder).txtnama.setText(modelpost.getTitle());

            if(modelpost.getAsset().size()!=0) {
                Picasso.with(context).load(BuildConfig.BASE_API_URL + "/" + modelpost.getAsset().get(0).getFile().replace("\\", "/"))
                        .error(R.drawable.loadimage)
                        .placeholder(R.drawable.loadimage)
                        .into(((ViewHolder) viewHolder).imgThumbnail);
            }
            //getdata
            ((ViewHolder) viewHolder).currentItem = listPost.get(i);
        }
        else {

            flagprog = 0;
            Log.d("koneksi", String.valueOf(fragment.flagkoneksi));
            if(fragment.flagkoneksi == 1) {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.GONE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.VISIBLE);
            }
            else
            {
                ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
                ((ProgressViewHolder) viewHolder).progressBar.setVisibility(View.VISIBLE);
                ((ProgressViewHolder) viewHolder).img.setVisibility(View.GONE);
            }

        }
    }

    public void addItem(Post post, int index) {
        listPost.add(post);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        listPost.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return listPost.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }

    public void setProgressOffSpan() {
        flagprog = 1;
    }

    public void setProgressOnSpan() {
        flagprog = 0;
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imgThumbnail;
        public TextView txtnama;

        public Post currentItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.gambar_card);
            txtnama = (TextView)itemView.findViewById(R.id.nama);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(view.getContext(), DetilExtraActivity.class);

                    Bundle b = new Bundle();
                    b.putString("id", String.valueOf(currentItem.getId()));
                    b.putString("judul", String.valueOf(currentItem.getTitle()));
                    b.putString("deskripsi", String.valueOf(currentItem.getContent()));
                    b.putString("status", String.valueOf(currentItem.getStatus()));
                    b.putString("tgl_create", String.valueOf(currentItem.getCreated_at()));
                    b.putString("size", String.valueOf(currentItem.getAsset().size()));
                    for(int i = 0 ; i< currentItem.getAsset().size(); i++)
                    {
                        b.putString("asset"+String.valueOf(i), currentItem.getAsset().get(i).getFile());
                    }

                    intent.putExtras(b);

                    ActivityOptionsCompat options =  ActivityOptionsCompat.makeSceneTransitionAnimation(activity, imgThumbnail, "detil" );

                    ActivityCompat.startActivity(activity, intent, options.toBundle());
                    //context.startActivity(i);

                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ImageView img;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
            img = (ImageView) v.findViewById(R.id.img);

            imgprog = img;
            progressprog = progressBar;

            img.setVisibility(View.GONE);
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    fragment.deleteItem();
                    loading = true;
                }
            });
        }
    }
}


