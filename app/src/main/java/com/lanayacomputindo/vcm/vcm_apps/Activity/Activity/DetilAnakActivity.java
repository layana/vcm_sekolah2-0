package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Helper.TimeFormater;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;

public class DetilAnakActivity extends AppCompatActivity {

    private TextView txtnama, txtnis, txtkelas, txttanggal;

    private ImageView imgphoto;

    private String bundlecabangid, bundlekelasid, bundleid,bundlenama,bundlenis, bundlephoto, bundlenamamember, bundletgl_lahir, bundlestatus, bundlekelas, bundlenamamerchant, bundletgl_create;

    static Activity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_anak);

        act = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("");

        txtnama = (TextView) findViewById(R.id.nama);
        txtnis = (TextView) findViewById(R.id.nis);
        txtkelas = (TextView) findViewById(R.id.kelas);
        txttanggal = (TextView) findViewById(R.id.tanggal);
        imgphoto = (ImageView) findViewById(R.id.photo);

        showData(getIntent().getExtras());

        imgphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), DetilGambarActivity.class);
//
//                Bundle b = new Bundle();
//
//                b.putString("photo", bundlephoto);
//                b.putString("namaproduk", bundlenama);
//                b.putString("param", "katalog");
//
//                i.putExtras(b);
//
//                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(DetilAnakActivity.this, imgphoto, "detil");
//                //Start the Intent
//                ActivityCompat.startActivity(DetilAnakActivity.this, i, options.toBundle());

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 123) { // Please, use a final int instead of hardcoded int value
            if (resultCode == RESULT_OK) {
                Log.e("bisa", "bisa");
                showData(data.getExtras());
            }
        }
    }

    private void showData(Bundle b) {
        bundleid = b.getString("id_anak");
        bundlenama = b.getString("nama_anak");
        bundlenis = b.getString("nis");
        bundlephoto = b.getString("photo");
        bundletgl_lahir = b.getString("tgl_lahir");
        bundlestatus = b.getString("status");
        bundlecabangid = b.getString("cabang_id");
        bundlekelasid = b.getString("kelas_id");
        bundlekelas = b.getString("nama_kelas");
        bundletgl_create = b.getString("tgl_create");

        txtnama.setText(bundlenama);
        txtnis.setText(bundlenis);
        txtkelas.setText(bundlekelas);
        //txttanggal.setText(bundletgl_lahir);
        txttanggal.setText(new TimeFormater().formattedDateFromString("yyyy-MM-dd", "dd MMM yyyy", bundletgl_lahir));

        Picasso.with(getApplicationContext()).load(BuildConfig.BASE_API_URL + "/" + bundlephoto)
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(imgphoto);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_detil_anak, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                DetilAnakActivity.this.supportFinishAfterTransition();

                return true;

            case R.id.menu_edit_anak:

                Intent i = new Intent(DetilAnakActivity.this, TambahAnakActivity.class);

                Bundle b = new Bundle();
                b.putString("id_anak", bundleid);
                b.putString("nama_anak", bundlenama);
                b.putString("nis", bundlenis);
                b.putString("photo", bundlephoto);
                b.putString("nama_member", bundlenamamember);
                b.putString("tgl_lahir", bundletgl_lahir);
                b.putString("status", bundlestatus);
                b.putString("cabang_id", bundlecabangid);
                b.putString("kelas_id", bundlekelasid);
                b.putString("nama_kelas", bundlekelas);
                b.putString("nama_merchant", bundlenamamerchant);
                b.putString("tgl_create", bundletgl_create);

                i.putExtras(b);

                startActivityForResult(i, 123);

                return true;

        }
        return super.onOptionsItemSelected(item);
    }

}
