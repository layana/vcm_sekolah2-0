package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Subject;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarMataPelajaranActivity extends AppCompatActivity {

    private BetterSpinner splevel, spclass, spsuject;

    private Button btnchat;

    private ProgressDialog pDialog;

    private Call<Results<Subject>> call;
    private RestClient.GitApiInterface service;

    private ArrayList<String> listNameSubject = new ArrayList<String>();
    private ArrayList<Integer> listIdSubject = new ArrayList<Integer>();

    private int idSubject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_mata_pelajaran);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setTitle("Guru 18 Jam");

        splevel = (BetterSpinner) findViewById(R.id.spinnerlevel);
        spclass = (BetterSpinner) findViewById(R.id.spinnerclass);
        spsuject = (BetterSpinner) findViewById(R.id.spinnersubject);
        String[] list = getResources().getStringArray(R.array.level);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);

        splevel.setAdapter(adapter);
        splevel.setText("SD");

        list = getResources().getStringArray(R.array.sd);

        adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, list);
        spclass.setAdapter(adapter);
        spclass.setText("I");
        getSubject();

        btnchat = (Button) findViewById(R.id.btnchat);
        btnchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(idSubject!=0) {
                    Intent i = new Intent(getApplicationContext(), DaftarGuruOnlineActivity.class);
                    Bundle b = new Bundle();
                    b.putString("id_subject", String.valueOf(idSubject));
                    b.putString("name_subject", spsuject.getText().toString());
                    b.putString("class", spclass.getText().toString());
                    i.putExtras(b);
                    startActivity(i);
                }
            }
        });

        splevel.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String[] list = {""};
                if(splevel.getText().toString().equalsIgnoreCase("SD")) {
                    list = getResources().getStringArray(R.array.sd);
                    spclass.setText("I");
                }
                if(splevel.getText().toString().equalsIgnoreCase("SMP")) {
                    list = getResources().getStringArray(R.array.smp);
                    spclass.setText("VII");
                }
                if(splevel.getText().toString().equalsIgnoreCase("SMA")) {
                    list = getResources().getStringArray(R.array.sma);
                    spclass.setText("X");
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.spinner_item, list);
                spclass.setAdapter(adapter);

                getSubject();
            }
        });

        spclass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                getSubject();
            }
        });
        spsuject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idSubject = listIdSubject.get(position);
            }
        });
    }

    public void getSubject()
    {
        pDialog = ProgressDialog.show(DaftarMataPelajaranActivity.this,
                "Mengambil Data Mata Pelajaran",
                "Tunggu Sebentar!");

        service = RestClient.getClient(this);
        String level = "";
        if(splevel.getText().toString().equalsIgnoreCase("SD"))
        {
            level = "primaryschool";
        }
        if(splevel.getText().toString().equalsIgnoreCase("SMP"))
        {
            level = "juniorhighschool";
        }
        if(splevel.getText().toString().equalsIgnoreCase("SMA"))
        {
            level = "seniorhighschool";
        }
        String search = "level:"+level + ";type:chat";
        call = service.getSubject(search);
        Log.d("level", level + splevel.getText().toString());
        call.enqueue(new Callback<Results<Subject>>() {
            @Override
            public void onResponse(Call<Results<Subject>> call, Response<Results<Subject>> response) {
                pDialog.dismiss();
                listNameSubject.clear();
                listIdSubject.clear();
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Subject> result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
                    for(Subject subject : result.getData())
                    {
                        listNameSubject.add(subject.getName());
                        listIdSubject.add(subject.getId());
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.spinner_item, listNameSubject);

                    spsuject.setAdapter(adapter);
                    if(listNameSubject.size()>0){
                        spsuject.setText(listNameSubject.get(0));
                    }
                    else
                    {
                        spsuject.setText("Tidak ada mata pelajaran");
                    }
                    if(listIdSubject.size()>0){
                        idSubject = listIdSubject.get(0);
                    }
                    else
                    {
                        idSubject = 0;
                    }

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors
                }
            }

            @Override
            public void onFailure(Call<Results<Subject>> call, Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                finish();
                Toast.makeText(getApplicationContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
