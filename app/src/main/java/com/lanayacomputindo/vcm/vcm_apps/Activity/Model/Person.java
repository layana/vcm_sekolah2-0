package com.lanayacomputindo.vcm.vcm_apps.Activity.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Pradana on 30/08/2016.
 */
public class Person {
    private Integer id;
    private Integer branch_id;
    private String type;
    private String name;
    private String email;
    private String departement;
    private String birthday;
    private String nip;
    private String phone;
    private String address;
    private String education;
    private String photo;
    private Boolean status;
    private String created_at;
    private String updated_at;
    private Object deleted_at;
    private Branch branch;
    private List<PersonUser> person_user = new ArrayList<PersonUser>();
    private List<PersonClass> person_class = new ArrayList<PersonClass>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The branch_id
     */
    public Integer getBranch_id() {
        return branch_id;
    }

    /**
     *
     * @param branch_id
     * The branch_id
     */
    public void setBranch_id(Integer branch_id) {
        this.branch_id = branch_id;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The departement
     */
    public String getDepartement() {
        return departement;
    }

    /**
     *
     * @param departement
     * The departement
     */
    public void setDepartement(String departement) {
        this.departement = departement;
    }

    /**
     *
     * @return
     * The nip
     */
    public String getNip() {
        return nip;
    }

    /**
     *
     * @param nip
     * The nip
     */
    public void setNip(String nip) {
        this.nip = nip;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The address
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     * The education
     */
    public String getEducation() {
        return education;
    }

    /**
     *
     * @param education
     * The education
     */
    public void setEducation(String education) {
        this.education = education;
    }

    /**
     *
     * @return
     * The photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     *
     * @param photo
     * The photo
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

    /**
     *
     * @return
     * The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The created_at
     */
    public String getCreated_at() {
        return created_at;
    }

    /**
     *
     * @param created_at
     * The created_at
     */
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    /**
     *
     * @return
     * The updated_at
     */
    public String getUpdated_at() {
        return updated_at;
    }

    /**
     *
     * @param updated_at
     * The updated_at
     */
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    /**
     *
     * @return
     * The deleted_at
     */
    public Object getDeleted_at() {
        return deleted_at;
    }

    /**
     *
     * @param deleted_at
     * The deleted_at
     */
    public void setDeleted_at(Object deleted_at) {
        this.deleted_at = deleted_at;
    }

    /**
     *
     * @return
     * The branch
     */
    public Branch getBranch() {
        return branch;
    }

    /**
     *
     * @param branch
     * The branch
     */
    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    /**
     *
     * @return
     * The person_user
     */
    public List<PersonUser> getPerson_user() {
        return person_user;
    }

    /**
     *
     * @param person_user
     * The person_user
     */
    public void setPerson_user(List<PersonUser> person_user) {
        this.person_user = person_user;
    }

    /**
     *
     * @return
     * The person_class
     */
    public List<PersonClass> getPerson_class() {
        return person_class;
    }

    /**
     *
     * @param person_class
     * The person_class
     */
    public void setPerson_class(List<PersonClass> person_class) {
        this.person_class = person_class;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
