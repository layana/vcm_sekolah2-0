package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.lanayacomputindo.vcm.vcm_apps.Activity.Activity.DetilGambarActivity;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;
import com.squareup.picasso.Picasso;


public class FragmentImage extends Fragment {


    private String photo, nama;

    private ImageView img;

    public FragmentImage() {
        // Required empty public constructor
    }

    public static FragmentImage newInstance(String param1, String nama) {
        FragmentImage fragment = new FragmentImage();
        Bundle args = new Bundle();
        args.putString("photo", param1);
        args.putString("nama", nama);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            photo = getArguments().getString("photo");
            nama = getArguments().getString("nama");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        img = (ImageView) view.findViewById(R.id.photo);
        Picasso.with(getActivity()).load(BuildConfig.BASE_API_URL + "/" + photo.replace("\\", "/"))
                .error(R.drawable.loadimage)
                .placeholder(R.drawable.loadimage)
                .into(img);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), DetilGambarActivity.class);

                Bundle b = new Bundle();

                b.putString("photo", photo);
                b.putString("namaproduk", nama);

                i.putExtras(b);

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), img, "detil");
                //Start the Intent
                ActivityCompat.startActivity(getActivity(), i, options.toBundle());

            }
        });
        return view;
    }

    public String getphoto() {
        return photo;
    }

}
