package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.widget.TextView;

import com.lanayacomputindo.vcm.vcm_apps.R;

public class DetilKalenderActivity extends AppCompatActivity {

    private String bundleid, bundlejudul, bundledeskripsi, bundletgl, bundlestatus, bundlenamamerchant, bundletanggal;

    private TextView txtjudul, txttanggal, txtdeskripsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detil_kalender);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        Bundle b = getIntent().getExtras();
        bundleid = b.getString("id_kalender");
        bundlejudul = b.getString("judul");
        bundledeskripsi = b.getString("deskripsi");
        bundletgl = b.getString("tgl_mulai");
        bundlestatus = b.getString("status");
        bundlenamamerchant = b.getString("nama_merchant");
        bundletanggal = b.getString("tgl_create");


        getSupportActionBar().setTitle(bundlejudul);

        txtjudul = (TextView) findViewById(R.id.judul);
        txttanggal = (TextView) findViewById(R.id.tanggal);
        txtdeskripsi = (TextView) findViewById(R.id.deskripsi);

        txtjudul.setText(bundlejudul);
        txttanggal.setText(bundletanggal);

        Spanned sp = Html.fromHtml(bundledeskripsi);
        txtdeskripsi.setText(sp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {

            case android.R.id.home:

                DetilKalenderActivity.this.supportFinishAfterTransition();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
