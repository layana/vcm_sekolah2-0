package com.lanayacomputindo.vcm.vcm_apps.Activity.Fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Adapter.ListOperasionalAdapter;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Person;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Results;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yoshua on 6/21/2015.
 */
public class FragmentOperasional extends Fragment implements SearchView.OnQueryTextListener {

    RecyclerView mRecyclerView;
    GridLayoutManager  mLayoutManager;
    private ListOperasionalAdapter mAdapter;

    SwipeRefreshLayout mSwipeRefreshLayout ;

    private ArrayList<Person> listguru = new ArrayList<Person>();

    Call<Results<Person>> call;
    RestClient.GitApiInterface service;

    LinearLayout Progress;
    ImageView progressimage, notfound;

    private int limit = 10;
    int flagmasihadadata = 0;

    private SearchView mSearchView;

    public int flagkoneksi = 0;

    ArrayList<Person> listPertama;

    View v;

    int branch_id;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getActivity().getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            branch_id = sharedPreferences.getInt("branch_id", 0);
        } else {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_recycleview, container, false);

        loadPreferences();

        Progress = (LinearLayout) v.findViewById(R.id.progress);
        progressimage = (ImageView) v.findViewById(R.id.progressimage);
        notfound = (ImageView) v.findViewById(R.id.notfound);
        notfound.setVisibility(View.GONE);

        // Calling the RecyclerView
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        mRecyclerView.setHasFixedSize(true);

        // The number of Columns
        mLayoutManager = new GridLayoutManager(getActivity(), 2);

        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                //Log.d("flagprog",String.valueOf(mAdapter.flagprog));
                if(mAdapter.flagprog == 0) {

                    mAdapter.setProgressOffSpan();
                    return 2;
                }
                else {
                    return 1;
                }
            }
        });

        mRecyclerView.setLayoutManager(mLayoutManager);

        loadDataFromServer();

        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                mSwipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.warna_utama,
                R.color.warna_utama,
                R.color.warna_utama);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Refreshing data on server
                loadDataFromServer();
            }
        });

        return v;
    }

    public void loadDataFromServer()
    {
        flagmasihadadata = 0;
        listguru.clear();
        service = RestClient.getClient(getActivity());
        String search = "branch_id:"+branch_id+";type:operational";
        String searchFields = "branch_id:=;type:=";
        call = service.getPerson(search, searchFields);

        call.enqueue(new Callback<Results<Person>>() {
            @Override
            public void onResponse(Call<Results<Person>> call, Response<Results<Person>> response) {
                flagkoneksi = 0;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("Load Pengajar Pertama", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)

                    Results<Person> result = response.body();
                    Log.d("Load Pengajar Pertama", "response = " + new Gson().toJson(result));
                    listguru = result.getData();

                    listPertama = result.getData();

                    Log.d("key", String.valueOf(result.getData().size()));

                    mAdapter = new ListOperasionalAdapter(v.getContext(), listguru, mRecyclerView, FragmentOperasional.this, getActivity());
                    mAdapter.setLoaded();
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Person>> call,Throwable t) {

                flagkoneksi = 1;
                mSwipeRefreshLayout.setRefreshing(false);
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();


            }
        });
    }

    public void deleteItem()
    {
        listguru.remove(listguru.size() - 1);
        mAdapter.deleteItem(listguru.size());
    }

    public void getGuruCari(String cari)
    {
        service = RestClient.getClient(getActivity());
        String search = "branch_id:"+branch_id+";type:operational;name:"+cari;
        String searchFields = "branch_id:=;type:=;name:like";
        call = service.getPerson(search, searchFields);

        call.enqueue(new Callback<Results<Person>>() {
            @Override
            public void onResponse(Call<Results<Person>> call,Response<Results<Person>> response) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Results<Person>result = response.body();
                    Log.d("MainActivity", "response = " + new Gson().toJson(result));
                    listguru = result.getData();
                    mAdapter = new ListOperasionalAdapter(v.getContext(), listguru, mRecyclerView, FragmentOperasional.this, getActivity());
                    mRecyclerView.setAdapter(mAdapter);

                } else {
                    // response received but request not successful (like 400,401,403 etc)
                    //Handle errors

                }
            }

            @Override
            public void onFailure(Call<Results<Person>> call,Throwable t) {
                Progress.setVisibility(View.GONE);
                progressimage.setVisibility(View.GONE);
                notfound.setVisibility(View.GONE);
                Log.d("MainActivity", "gagalall");
                Toast.makeText(v.getContext(), R.string.cekkoneksi, Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_katalog, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchItem.getActionView();
        setupSearchView(searchItem);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupSearchView(MenuItem searchItem) {

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        if (searchManager != null) {

            mSearchView.setQueryHint("Cari Pegawai");
            View searchPlate = mSearchView.findViewById(android.support.v7.appcompat.R.id.search_plate);
            searchPlate.setBackgroundResource(R.drawable.abc_textfield_search_material);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        }

        mSearchView.setOnQueryTextListener(this);
    }

    public boolean onQueryTextChange(String query) {

        getGuruCari(query);

        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        String keyword = query;

        getGuruCari(keyword);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
        return false;
    }

}
