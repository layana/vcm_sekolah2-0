package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Result;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.Token;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Model.User;
import com.lanayacomputindo.vcm.vcm_apps.Activity.Rest.RestClient;
import com.lanayacomputindo.vcm.vcm_apps.BuildConfig;
import com.lanayacomputindo.vcm.vcm_apps.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifikasiActivity extends AppCompatActivity {

    private TextView back, txtemail, txtnama;
    ProgressDialog pDialog;

    private String email, password, nama;

    private SharedPreferences sharedPreferences;

    public void loadPreferences() {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        if (sharedPreferences != null) {
            email = sharedPreferences.getString("emaildaftar", "");
            password = sharedPreferences.getString("passworddaftar", "");
            nama = sharedPreferences.getString("namadaftar", "");
        } else {

        }
    }

    private void savePreferences(boolean islogin,
                                 int id,
                                 int client_id,
                                 String type,
                                 String name,
                                 String email,
                                 String birthday,
                                 String religion,
                                 String phone,
                                 String address,
                                 String photo,
                                 boolean status,
                                 String created_at,
                                 String updated_at) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("islogin", islogin);
        editor.putInt("id", id);
        editor.putInt("client_id", client_id);
        editor.putString("type", type);
        editor.putString("name", name);
        editor.putString("email", email);
        editor.putString("birthday", birthday);
        editor.putString("religion", religion);
        editor.putString("phone", phone);
        editor.putString("address", address);
        editor.putString("photo", photo);
        editor.putBoolean("status", status);
        editor.putString("created_at", created_at);
        editor.putString("updated_at", updated_at);
        editor.commit();
    }

    private void saveToken(String user_token) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("user_token", user_token);
        editor.commit();
    }

    private void saveVerify(boolean isverify) {
        sharedPreferences = getSharedPreferences("shared",
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isverify", isverify);
        editor.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifikasi);

        loadPreferences();
        saveVerify(true);

        getToken(email, password);

        txtemail = (TextView) findViewById(R.id.email);
        txtnama = (TextView) findViewById(R.id.nama);

        txtemail.setText("Email : " + email);
        txtnama.setText("Nama : " + nama);

        back = (TextView) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveVerify(false);
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    public void getToken(String email, String password)
    {
        pDialog = ProgressDialog.show(VerifikasiActivity.this,
                "Verifikasi Pengguna",
                "Tunggu Sebentar!");
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<Token>> call = service.getToken(BuildConfig.CLIENT_ID, email,password);
        call.enqueue(new Callback<Result<Token>>() {
            @Override
            public void onResponse(Call<Result<Token>> call, Response<Result<Token>> response) {
                Log.d("Login", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    Result<Token> result = response.body();
                    Log.d("Login", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        saveToken(result.getData().getToken());
                        Log.d("Login", "Token: " + response.body().getData().getToken());
                        getProfil();
                    }
                    else {
                        Log.e("Login", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Login", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Anda belum terverifikasi",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<Token>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getProfil()
    {
        RestClient.GitApiInterface service = RestClient.getClient(this);
        Call<Result<User>> call = service.getProfil();

        call.enqueue(new Callback<Result<User>>() {
            @Override
            public void onResponse(Call<Result<User>> call,Response<Result<User>> response) {
                Log.d("Get Profil", "Status Code = " + response.code());
                if (response.isSuccessful()) {
                    Result<User> result = response.body();
                    Log.d("Get Profil", "response = " + new Gson().toJson(result));
                    if(result.getSuccess())
                    {
                        savePreferences(true,
                                result.getData().getId(),
                                result.getData().getClient_id(),
                                result.getData().getType(),
                                result.getData().getName(),
                                result.getData().getEmail(),
                                result.getData().getBirthday(),
                                result.getData().getReligion(),
                                result.getData().getPhone(),
                                result.getData().getAddress(),
                                result.getData().getPhoto(),
                                result.getData().getStatus(),
                                result.getData().getCreated_at(),
                                result.getData().getUpdated_at());

                        pDialog.dismiss();
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        LoginActivity.ActLogin.finish();
                        startActivity(i);
                        finish();

                    }
                    else {
                        Log.e("Get Profil", response.body().getMessage());
                        pDialog.dismiss();
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Log.e("Get Profil", String.valueOf(response.raw().toString()));
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Tidak Dapat",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Result<User>> call,Throwable t) {
                pDialog.dismiss();
                Log.e("on Failure", t.toString());
                Toast.makeText(getApplicationContext(),R.string.cekkoneksi,Toast.LENGTH_LONG).show();
            }
        });
    }
}
