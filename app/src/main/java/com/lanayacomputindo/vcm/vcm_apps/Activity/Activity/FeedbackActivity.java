package com.lanayacomputindo.vcm.vcm_apps.Activity.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lanayacomputindo.vcm.vcm_apps.R;

public class FeedbackActivity extends AppCompatActivity {

	private Button btnkirim;

	private EditText edjudul, edpesan;

	private String email;

	private int sudahkirim=0;

	private SharedPreferences sharedPreferences;

	public void loadPreferences() {
		sharedPreferences = getSharedPreferences("shared",
				Activity.MODE_PRIVATE);
		if (sharedPreferences != null) {
			email = sharedPreferences.getString("branch_email", "");
		} else {

		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);

		loadPreferences();

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final Drawable upArrow = getResources().getDrawable(R.drawable.ic_chevron_left);
		getSupportActionBar().setHomeAsUpIndicator(upArrow);
		getSupportActionBar().setTitle("Feedback");

		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		edjudul= (EditText) findViewById(R.id.judul);
		edpesan= (EditText) findViewById(R.id.pesan);

		btnkirim = (Button) findViewById(R.id.btnkirim);

		btnkirim.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(Intent.ACTION_SENDTO);
				intent.setData(Uri.parse("mailto:"+email));
				intent.putExtra(Intent.EXTRA_SUBJECT, edjudul.getText().toString());
				intent.putExtra(Intent.EXTRA_TEXT, edpesan.getText().toString());
				startActivity(Intent.createChooser(intent, "Pilih email anda"));

				sudahkirim = 1;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(sudahkirim == 1)
		{
			Toast.makeText(getApplicationContext(), "Terimakasih telah memberikan feedback kepada kami.", Toast.LENGTH_SHORT).show();
			finish();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {

			case android.R.id.home:

				FeedbackActivity.this.finish();

				return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
